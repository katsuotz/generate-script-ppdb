import pymongo
import xlrd
import datetime
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolRegis = mydb["ppdb_registrations_2020"]
mycolJunior = mydb["ppdb_unjuniors"]

regisArray = mycolRegis.find({"level_pendaftaran": "nhun", "tahap_pendaftaran": 2, "total_rapor": 0,
    "status": {"$in": ["new", "approved"]}})

for regis in regisArray:
    junior = mycolJunior.find_one({"_id": regis["id_junior"]})
    if "rapor" in junior:
        mycolRegis.update_one({
            "_id": regis["_id"]
        }, {
            "$set": {
                "rapor": junior["rapor"]
            }
        })

regisArray = mycolRegis.find({"level_pendaftaran": "nhun", "tahap_pendaftaran": 2, 
    "status": {"$in": ["new", "approved"]}})

i = 0
for regis in regisArray:
    if "rapor" in regis:
        i+=1
        print("Regis ", regis["code"], i)
        if len(regis["rapor"]) > 0:
            totalRapor = 0
            for rapor in regis["rapor"]:
                if "score_bahasa" in rapor:
                    if rapor["score_bahasa"] is not None:
                        totalRapor += float(rapor["score_bahasa"])
                if "score_english" in rapor:
                    if rapor["score_english"] is not None:
                        totalRapor += float(rapor["score_english"])
                if "score_math" in rapor:
                    if rapor["score_math"] is not None:
                        totalRapor += float(rapor["score_math"])
                if "score_physics" in rapor:
                    if rapor["score_physics"] is not None:
                        totalRapor += float(rapor["score_physics"])
                if "score_religion" in rapor:
                    if rapor["score_religion"] is not None:
                        totalRapor += float(rapor["score_religion"])
                if "score_pancasila" in rapor:
                    if rapor["score_pancasila"] is not None:
                        totalRapor += float(rapor["score_pancasila"])
                if "score_science" in rapor:
                    if rapor["score_science"] is not None:
                        totalRapor += float(rapor["score_science"])
            if totalRapor > 0:
                mycolRegis.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": {
                        "total_rapor": totalRapor
                    }
                })
            else:
                mycolRegis.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": {
                        "status": "reject",
                        "alasan": "Rapor harap diisi terlebih dahulu"
                    }
                })
        else:         
            mycolRegis.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": {
                        "status": "reject",
                        "alasan": "Rapor harap diisi terlebih dahulu"
                    }
                })
        