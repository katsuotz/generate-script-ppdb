import pymongo
import json
import datetime
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
mydb = myclient["ppdb"]
mycolJunior = mydb["ppdb_unjuniors"]
# mycolSchool = mydb["ppdb_schools"]
# mycolUser = mydb["ppdb_users"]
# mycolMajor = mydb["ppdb_majors"]
# mycolRegion = mydb["ppdb_regions"]


juniorData = mycolJunior.find({"nik": {"$exists": True}, "nik": { "$nin": [ None,""," "] } })

i=0
for junior in juniorData:
    print(junior['_id'])
    nik = junior['nik']
    nik = nik.encode('ascii', 'ignore').decode('unicode_escape')
    nik = nik.replace('\u200c', '')
    nik = nik.strip()
    if nik!="" and nik!="-" and len(nik) == 16 and nik != "2020020100030226":
        nik = nik.replace("'", "").replace(",", "")
        niksubstring = nik[6:12]
        gender=''
        tgl=""
        bln=""
        thn=""
        if int(niksubstring[0:2]) > 31:
            gender="F"
            tgl=str(int(niksubstring[0:2]) - 40)
            bln=(niksubstring[2:4])
            thn=(niksubstring[4:6])
        else:
            gender="M"
            tgl=(niksubstring[0:2])
            bln=(niksubstring[2:4])
            thn=(niksubstring[4:6])

        # print(junior['_id'], nik, nik[6:12], niksubstring[0:2], tgl, bln, thn)
        if thn[0:1] == "0":
            thn="20"+thn
        else:
            thn="19"+thn

        try :
            mycolJunior.update_one({"_id": junior['_id']}, {
                "$set": {
                    "birth_date": datetime.datetime(int(thn), int(bln), int(tgl), 0, 0),
                    "gender": gender
                }
            })
            i+=1
            print(i, junior['_id'], nik, nik[6:12], gender, tgl, bln, thn)
        except:
            print("Something error", junior['_id'])

print("Done")
