import pymongo
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolRegistrant = mydb["ppdb_registrations_2020"]

query=[{
    "$lookup": {
        "from": "ppdb_options",
        "localField": "first_choice",
        "foreignField": "_id",
        "as": "first_option"
    }
}, {
    "$unwind": "$first_option"
}, {   
    "$match": {
    }
}, {
      "$group" :
        {
          "_id": "$level_pendaftaran",
           "jumlah": {
                "$sum": {
                    "$cond": [{"$and" : [
                       { "$ne": ["$level_pendaftaran", "$first_option.type"]}
                    ]}, 1, 0]
                }
            },
            "pendaftar": {
                "$addToSet": {
                    "$cond": [{"$and" : [
                       { "$ne": ["$level_pendaftaran", "$first_option.type"]}
                    ]}, "$_id", ""]
                }
            },
        }
     }
]
registrant = mycolRegistrant.aggregate(query)

i=0
for regis in registrant:
    for idRegis in regis["pendaftar"]:
        if idRegis != "":
            i+=1
            mycolRegistrant.delete_one({
                "_id": idRegis
            })
            print(i)