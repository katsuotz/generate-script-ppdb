import pymongo
import json
import os
import datetime
from dateutil import relativedelta

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolJunior = mydb["ppdb_unjuniors"]
mycolRegistration = mydb["ppdb_registrations_2020"]

juniors = mycolJunior.find({"asal_sekolah": "indonesia_ln"})

for junior in juniors:
    mycolRegistration.update_one({
        "id_junior": junior["_id"]
    }, {
        "$set": {
            "daerah_asal_sekolah": junior["daerah_asal_sekolah"],
            "asal_sekolah": junior["asal_sekolah"],
        }
    })
    print("Done")