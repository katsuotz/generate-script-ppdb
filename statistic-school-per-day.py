import pymongo
import json
import datetime
import bson
from datetime import timedelta

#myclient = pymongo.MongoClient("mongodb://127.0.0.1:27017/")
myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
mydb = myclient["ppdb"]

registrantModel = mydb["ppdb_registrations_2020"]
statisticModel = mydb["ppdb_statistic_schools"]
statisticRegistrantModel = mydb["ppdb_statistic_registrants"]

current_date = datetime.date.today()

# print(current_date.year)
# print(current_date.month)
# print(current_date.day)
dayLimit = datetime.datetime(current_date.year, 6, 24, 0, 0)
start = datetime.datetime(current_date.year, current_date.month, current_date.day, 0, 0)
end = datetime.datetime(current_date.year, current_date.month, current_date.day, 23, 59)
#start = datetime.datetime(current_date.year, 6, 27, 0, 0)
#end = datetime.datetime(current_date.year, 6, 27, 23, 59)
#start = datetime.datetime(2020, 6, 4)
delta = start - dayLimit


#end = datetime.datetime(2020, 6, 9)

if delta.days >= 0:
    for i in range(delta.days):
        dayLimit = dayLimit + timedelta(days=1)
        start = datetime.datetime(dayLimit.year, dayLimit.month, dayLimit.day, 0, 0)
        end = datetime.datetime(dayLimit.year, dayLimit.month, dayLimit.day, 23, 59)
        
        query = [{
            '$match': {
                "status": "approved",
                "createdAt": {
                    '$gte': start,
                    '$lt': end
                }
            }
        }, {
            "$group": {
                "_id": {
                    "school_id": "$first_choice_school"
                },
                "count": {
                    "$sum": 1
                }
            }
        }]

        registrant = registrantModel.aggregate(query)
        statisticArray = []

        for i in registrant:
            print(i)
            statisticArray.append({
                "school_id": bson.ObjectId(i['_id']['school_id']),
                "count": i['count'],
                "date": start,
                "last_modified":datetime.datetime.now()
            })

        statisticModel.delete_many({"date": start})
        if len(statisticArray) > 0:
            statisticModel.insert_many(statisticArray)

        queryRekap = [
            {
            "$match" : {
                    "status": "approved",
            }
            },
            {
                "$lookup": {
                    "from": "ppdb_schools",
                    "localField": "first_choice_school",
                    "foreignField": "_id",
                    "as": "school"
                }
            },
            {
                "$group": {
                    "_id": "$first_choice_school",
                    "level": {
                        "$first": "$school.level"
                    },
                    "prestasi": {
                        "$sum": {
                            "$cond": [{
                                "$eq": ["$level_pendaftaran", "prestasi"]
                            }, 1, 0]
                        }
                    },
                    "nhun": {
                        "$sum": {
                            "$cond": [{
                                "$eq": ["$level_pendaftaran", "nhun"]
                            }, 1, 0]
                        }
                    },
                    "nhun_unggulan": {
                        "$sum": {
                            "$cond": [{
                                "$eq": ["$level_pendaftaran", "nhun-unggulan"]
                            }, 1, 0]
                        }
                    },
                    "abk": {
                        "$sum": {
                            "$cond": [{
                                "$eq": ["$level_pendaftaran", "abk"]
                            }, 1, 0]
                        }
                    },
                    "anak_guru": {
                        "$sum": {
                            "$cond": [{
                                "$eq": ["$level_pendaftaran", "anak-guru"]
                            }, 1, 0]
                        }
                    },
                    "ketm": {
                        "$sum": {
                            '$cond': [{
                                "$eq": ["$level_pendaftaran", "ketm"]
                            }, 1, 0]
                        }
                    },
                    "perpindahan": {
                        "$sum": {
                            "$cond": [{
                                "$eq": ["$level_pendaftaran", "perpindahan"]
                            }, 1, 0]
                        }
                    },
                    "prestasi": {
                        "$sum": {
                            "$cond": [{
                                "$eq": ["$level_pendaftaran", "prestasi"]
                            }, 1, 0]
                        }
                    },
                    "slb": {
                        "$sum": {
                            "$cond": [{
                                "$eq": ["$level_pendaftaran", "slb"]
                            }, 1, 0]
                        }
                    },
                    "zonasi": {
                        "$sum": {
                            "$cond": [{
                                "$eq": ["$level_pendaftaran", "zonasi-jarak"]
                            }, 1, 0]
                        }
                    },
                    "tenaga_kesehatan": {
                        "$sum": {
                            "$cond": [{
                                "$eq": ["$level_pendaftaran", "tenaga-kesehatan"]
                            }, 1, 0]
                        }
                    },
                    
                }
            }
        ]

        registrantRekap = registrantModel.aggregate(queryRekap)
        regisArray = []
        for regis in registrantRekap:

            if len(regis["level"]) > 0 and regis["level"][0] == "vocational":
            
                rekapSMK = [
                    {
                        "$match": {
                            "status": "approved",
                            "first_choice_school": regis["_id"]
                            
                        }
                    },	
                    {
                        "$group": {
                            "_id": "$first_choice",
                                        "count": {"$sum": 1},
                        }
                    },
                        {
                        "$lookup": {
                            "from": "ppdb_options",
                            "localField": "_id",
                            "foreignField": "_id",
                            "as": "options"
                        }
                    },
                        {
                        "$unwind": {
                            "path": "$options",
                            "preserveNullAndEmptyArrays": True
                        }
                    },
                    {
                        "$project": {
                            "_id": 1,
                            "count": 1,
                            "options.name": 1
                        
                        }
                    },
                ]
                optionSMK = []
                smk = registrantModel.aggregate(rekapSMK)
                for d in smk:
                    optionSMK.append(d)
                regis.update({"smk": optionSMK})
                regisArray.append(regis)
                print("rekap")
            else:
                regisArray.append(regis)

        statisticRegistrantModel.delete_many({})
        statisticRegistrantModel.insert_many(regisArray)

        myclient.close()
        #for a in statisticArray:
        print("Suksess")