import pymongo
import json
import os
import datetime
from dateutil import relativedelta

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolJunior = mydb["ppdb_unjuniors"]
mycolRegistration = mydb["ppdb_registrations_2020"]

registrant = mycolRegistration.find({})

# def calculateAge(birthDate): 
#     days_in_year = 365.2425    
#     age = int((datetime.date.today() - birthDate).days / days_in_year) 
#     return age 
          
i=0
for regis in registrant:
    if "birth_date" in regis:
        date_str = str(regis['birth_date'])
        format_str = '%Y-%m-%d %H:%M:%S' # The format
        datetime_obj = datetime.datetime.strptime(date_str, format_str)
        tgl_lahir = datetime_obj.date()
        tgl_akhir = datetime.datetime(2020, 7, 1)

        diff = relativedelta.relativedelta(tgl_akhir, tgl_lahir)

        years = diff.years
        months = diff.months
        days = diff.days    

        mycolRegistration.update_one({
            "_id": regis["_id"]
        }, {
            "$set": {
                "age": years,
                "age_month": months,
                "age_day": days
            }
        })

        i+=1
        print(i)