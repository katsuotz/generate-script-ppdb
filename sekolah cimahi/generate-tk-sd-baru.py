from dbfread import DBF
import pymongo
import xlrd
import json
import os

dataRapor=[]
myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
mydb = myclient["ppdbCimahi"]
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
#mydb = myclient["ppdb_cimahi"]
myUser = mydb["ppdb_users"]
mySchools = mydb["ppdb_schools"]

#dirname = os.path.join(os.path.dirname(os.path.abspath(__file__)), "TKSDBARU.xlsx")
dirname = os.path.join(os.path.dirname(os.path.abspath(__file__)), "TKBARULAGI.xlsx")
print(dirname)
wb = xlrd.open_workbook(dirname) 
sh = wb.sheet_by_index(0)


index=0
for i in range(sh.nrows):
    if i >= 3:
        value = sh.row_values(i)
        rapor=[]
        level = value[2]
        mySchools.delete_one({"name": value[1]})
        myUser.delete_one({"name": value[1]})
        dataRapor.append({
            "type": "private",
            "level": level,
            # "code": codeSchool,
            "school_un_smp": "",
            "name":value[1],
            "address": "",
            "address_city": "",
            "address_province": "JAWA BARAT",
            "address_district": "",
            "address_subdistrict": "",
            "address_rw": "00",
            "address_rt": "00",
            "coordinate_lat": "0.0",
            "coordinate_lng": "0.0",
            "permission": "000",
            "school_un": "0",
            "npsn": str(value[0]),
        })
mySchools.insert_many(dataRapor)

schools = mySchools.find({})

for sc in schools:
    user = myUser.find_one({"school": str(sc["_id"])})
    if user is None:
        if sc["level"] == "elementary":
            myUser.insert_one({
                "name": sc["name"],
                "username": "CMHSD"+sc["npsn"],
                "password": "CMHSD"+sc["npsn"],
                "role": "elementaryschool-admin",
                "school": str(sc["_id"])
            })
        elif sc["level"] == "junior":
            myUser.insert_one({
                "name": sc["name"],
                "username": "CMHSMP"+sc["npsn"],
                "password": "CMHSMP"+sc["npsn"],
                "role": "juniorschool-admin",
                "school": str(sc["_id"])
            })
        elif sc["level"] == "tk":
            myUser.insert_one({
                "name": sc["name"],
                "username": "TK"+sc["npsn"],
                "password": "TK"+sc["npsn"],
                "role": "tkschool-admin",
                "school": str(sc["_id"])
            })