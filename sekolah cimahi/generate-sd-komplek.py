from dbfread import DBF
import pymongo
import xlrd
import json
import os

dataRapor=[]
myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
mydb = myclient["ppdbCimahi"]
#myclient = pymongo.MongoClient("mongodb://ppdbCimahiDev:1@localhost:27017/ppdbCimahiDev")
#mydb = myclient["ppdbCimahiDev"]
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
#mydb = myclient["ppdb_cimahi"]
myUser = mydb["ppdb_users"]
mySchools = mydb["ppdb_schools"]
myOptions = mydb["ppdb_options"]

dirname = os.path.join(os.path.dirname(os.path.abspath(__file__)), "SDKOMPLEKFIX.xlsx")
print(dirname)
wb = xlrd.open_workbook(dirname) 
sh = wb.sheet_by_index(0)


index=0
for i in range(sh.nrows):
    if i >= 3:
        value = sh.row_values(i)
        rapor=[]
        sekolahChild=value[3].split(',')

        sekolahChildId = []

        for s in sekolahChild:
            school=mySchools.find_one({"name": s})
            sekolahChildId.append(school['_id'])
            mySchools.update_one(
                {"name": s},
                {
                    "$set": {
                        "komplek": True
                    }
                }
            )
            opt=myOptions.delete_many({"school_id": school['_id']})
            #opt delete

        #mySchools.delete_one({"name": value[2]})
        #myUser.delete_one({"name": value[2]})
        dataRapor.append({
            "type": "open",
            "level": "elementary",
            # "code": codeSchool,
            "school_un_smp": "",
            "name":value[2],
            "address": "",
            "address_city": "KOTA CIMAHI",
            "address_province": "JAWA BARAT",
            "address_district": "",
            "address_subdistrict": "",
            "address_rw": "00",
            "address_rt": "00",
            "coordinate_lat": "0.0",
            "coordinate_lng": "0.0",
            "permission": "000",
            "school_un": "0",
            "npsn": str(value[4]),
            "school_children": sekolahChildId
        })

#print(dataRapor)

mySchools.insert_many(dataRapor)

schools = mySchools.find({"level": "elementary"})

for sc in schools:
    user = myUser.find_one({"school": str(sc["_id"])})
    if user is None:
        myUser.insert_one({
            "name": sc["name"],
            "username": "CMHSD"+sc["npsn"],
            "password": "CMHSD"+sc["npsn"],
            "role": "elementaryschool-admin",
            "school": str(sc["_id"])
        })