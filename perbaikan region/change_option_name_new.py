import pymongo
import xlrd
import datetime
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolRegion = mydb["ppdb_regions"]
mycolSchool = mydb["ppdb_schools"]
mycolJunior = mydb["ppdb_unjuniors"]

dirname ="./different_sub_district_new.xlsx"
print(dirname)
wb = xlrd.open_workbook(dirname) 
sh = wb.sheet_by_index(0)
index=0
for i in range(sh.nrows):
    if i >= 1:
        value = sh.row_values(i)
        
        print("REGION \n\n")
        mycolRegion.update_one({
            "province": "JAWA BARAT",
            "city": value[0],
            "district": value[1],
            "subdistrict": value[2],
        }, {
            "$set": {
                "subdistrict": value[5],
            }
        })
        
        print("Sekolah \n\n")
        mycolSchool.update_many({
            "address_province": "JAWA BARAT",
            "address_city": value[0],
            "address_district": value[1],
            "address_subdistrict": value[2],
        }, {
            "$set": {
                "address_subdistrict": value[5],
            }
        })
        
        
        print("Junior \n\n")
        mycolJunior.update_many({
            "address_province": "JAWA BARAT",
            "address_city": value[0],
            "address_district": value[1],
            "address_subdistrict": value[2],
        }, {
            "$set": {
                "address_subdistrict": value[5],
            }
        })
        
        print(value[5])
  