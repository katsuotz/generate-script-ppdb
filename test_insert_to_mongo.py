import pymongo
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
mydb = myclient["ppdb"]
# mycolJunior = mydb["ppdb_unjuniors"]
# mycolSchool = mydb["ppdb_schools"]
# mycolUser = mydb["ppdb_users"]
# mycolMajor = mydb["ppdb_majors"]
mycolRegion = mydb["ppdb_regions"]

regionData = mycolRegion.find({"code_new_subdistrict": {"$exists": True}})

for region in regionData:
    code_new_subdistrict=(int(region['code_new_subdistrict'])+1)

    region_code=region["code"]
    region_code=region_code[:-1]

    region_code = region_code + str(code_new_subdistrict)

    mycolRegion.update_one({"_id": region["_id"]}, {
        "$set" : {
            "code_new_subdistrict": code_new_subdistrict,
            "code": region_code
        }
    })
    print(region["_id"])

# juniorData = mycolJunior.find({"document": {"$exists": True}, "document.level_pendaftaran": "ketm", "document.ketm.nik_kepala": {"$exists": True}, "document.ketm.nama_kepala":{ "$regex": "^3"} }, no_cursor_timeout=True).batch_size(100)

# i=0
# for junior in juniorData:
#     mycolJunior.update_one({"_id": junior["_id"]}, {
#         "$set" : {
#             "document.ketm.nik_kepala": junior["document"]["ketm"]["nama_kepala"],
#             "document.ketm.nama_kepala": junior["document"]["ketm"]["nik_kepala"],
#         }
#     })
#     i+=1
#     print(junior["_id"])

# dataSchool = mycolSchool.find({"paket_b": True, "level": "junior", "school_un": {"$exists": True}})
# def getaddresscity(kd_rayon):
#     switcher={
#         1: 'KOTA BANDUNG',
#         2: "KOTA BANJAR",
#         3: "KOTA BEKASI",
#         4: "KOTA BOGOR",
#         5: "KOTA CIMAHI",
#         6: "KOTA CIREBON",
#         7: "KOTA DEPOK",
#         8: "KOTA SUKABUMI",
#         9: "KOTA TASIKMALAYA",
#         10: "KAB. BANDUNG",
#         11: "KAB. BANDUNG BARAT",
#         12: "KAB. BEKASI",
#         13: "KAB. BOGOR",
#         14: "KAB. CIAMIS",
#         15: "KAB. CIANJUR",
#         16: "KAB. CIREBON",
#         17: "KAB. GARUT",
#         18: "KAB. INDRAMAYU",
#         19: "KAB. KARAWANG",
#         20: "KAB. KUNINGAN",
#         21: "KAB. MAJALENGKA",
#         22: "KAB. PURWAKARTA",
#         23: "KAB. SUBANG",
#         24: "KAB. SUKABUMI",
#         25: "KAB. SUMEDANG",
#         26: "KAB. TASIKMALAYA",
#         27: "KAB. PANGANDARAN",
#     }
#     return switcher.get(kd_rayon, "-")

# i=0
# for school in dataSchool:
#     schoolUn = school['school_un'].split("-")
#     if schoolUn[0] == "02":
#         mycolSchool.update_one({"_id": school["_id"]}, {
#             "$set": {
#                 "address_city": getaddresscity(int(schoolUn[1]))
#             }
#         })
#         i+=1
#         print(i)
      
# schoolSenior = mycolSchool.find({"level": "senior"}, no_cursor_timeout=True).batch_size(300)
# schoolVocational = mycolSchool.find({"level": "vocational"}, no_cursor_timeout=True).batch_size(300)
# schoolSLB = mycolSchool.find({"level": "slb"}, no_cursor_timeout=True).batch_size(300)
# schoolJUNIOR = mycolSchool.find({"level": "junior"}, no_cursor_timeout=True).batch_size(300)

# i=1
# for school in schoolSenior:
#     schoolCode="3"+str(i).zfill(5)
#     mycolSchool.update_one({"_id": school["_id"]}, {
#         "$set": {
#             "code" :int(schoolCode)
#         }
#     })
#     i+=1
#     print("Senior ",i)

# i=1
# for school in schoolVocational:
#     schoolCode="4"+str(i).zfill(5)
#     mycolSchool.update_one({"_id": school["_id"]}, {
#         "$set": {
#             "code" :int(schoolCode)
#         }
#     })
#     i+=1
#     print("Vocational ",i)

# i=1
# for school in schoolSLB:
#     schoolCode="5"+str(i).zfill(5)
#     mycolSchool.update_one({"_id": school["_id"]}, {
#         "$set": {
#             "code" :int(schoolCode)
#         }
#     })
#     i+=1
#     print("SLB ",i)

# i=1
# for school in schoolJUNIOR:
#     schoolCode="6"+str(i).zfill(5)
#     mycolSchool.update_one({"_id": school["_id"]}, {
#         "$set": {
#             "code" :int(schoolCode)
#         }
#     })
#     i+=1
#     print("Junior ",i)
# list_major = []
# major = mycolMajor.find({})
# for data in major:
#     list_major.append(data)

# with open('major.txt', 'w') as outfile:
#     json.dump(list_major, outfile)


# dataJunior = mycolJunior.find({"un_year": "2020"})
# i=0
# for data in dataJunior:
#     mycolUser.update_one({'id_unjunior': data['_id']}, {
#         "$set": {
#             "school": str(data['school_id'])
#         }
#     })
#     i+=1
    # print(i)

# dataSchool = mycolSchool.find({}, no_cursor_timeout=True).batch_size(30)
# i=0
# for school in dataSchool:
#     mycolSchool.update_one({'_id': school['_id']}, {
#         "$set": {
#             "code": int(school['code'])
#         }
#     })
#     i+=1
#     print(i)

# dataJunior = mycolJunior.find({"un_year": {"$in" : ["2019", "2020"]},"un_type": "reguler smp", "school_id": {"$exists": False}}, no_cursor_timeout=True).batch_size(1000)
# i = 0
# for junior in dataJunior:
#     un = junior['un'].split("-")
#     unSplit = "^"+un[0]+"-"+un[1]+"-"+un[2]
#     query = { "school_un": { "$regex": unSplit}}
#     school = mycolSchool.find_one(query)
    
#     if school is not None :
#         mycolJunior.update_one({'_id': junior['_id']}, {
#             "$set": {
#                 "school_id": school['_id'],
#                 "school": school['name']
#             }
#         })
#     i+=1
#     print(i)

# dataJunior = mycolJunior.find({"un_year": {"$in" : ["2019", "2020"]},"un_type": "paket b","school_id": {"$exists": False}}, no_cursor_timeout=True).batch_size(1400)
# i = 0
# for junior in dataJunior:
#     un = junior['un'].split("-")
#     unSplit = "^"+un[0]+"-"+un[1]+"-"+un[2]
#     query = { "school_un": { "$regex": unSplit}, "paket_b": {"$exists": True}}
#     school = mycolSchool.find_one(query)
    
#     if school is not None :
#         mycolJunior.update_one({'_id': junior['_id']}, {
#             "$set": {
#                 "school_id": school['_id'],
#                 "school": school['name']
#             }
#         })
#     i+=1
#     print(i)
# print("Done")



# with open("data_school_PAKETB_JABAR.txt") as json_file:
#     data = json.load(json_file)
#     # x = mycolSchool.insert_many(data)
#     # print(x.inserted_ids)
#     i=0
#     for s in data:
#         school = mycolSchool.update_one({"name": s['name'], "school_un": s['school_un']}, {
#             "$set": {
#                 "paket_b": True
#             }
#         })
#         i+=1
#         print(i)
#     print(len(data))

# list_unjuniors = []

# with open("unjuniors_2020_temp.json") as json_file:
    # data = json.load(json_file)
  
    # for p in data["RECORDS"]:
    #     list_unjuniors.append({
    #         "un": p["un"],
    #         "un_year": "2020",
    #         "un_type": p['un_type'],
    #         "nisn": p["nisn"],
    #         "no_induk": p["no_induk"],
    #         "name": p["name"],
    #         "gender": p['gender'],
    #         "birth_place": p['birth_place'],
    #         "birth_date": p['birth_date'],
    #         "address": p['address'],
    #         "npsn": p["npsn"],
    #         "score_bahasa": p['score_bahasa'],
    #         "score_english": p['score_english'],
    #         "score_math": "0",
    #         "score_physics": p['score_physics'],
    #         "school": p['school_name']['name']
    #     })
    #     print(p["un"])
    # with open('data_unjuniors_fix.txt', 'w') as outfile:
    #     json.dump(list_unjuniors, outfile)
    # print("Done")
