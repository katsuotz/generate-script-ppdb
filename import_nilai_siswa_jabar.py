import pymongo
import xlrd
import datetime
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycoljunior = mydb["ppdb_unjuniors"]

def checkValidationScore(score):
    if score == '':
        return 0
    else:
        return int(score)

dirname = os.path.join(os.path.dirname(os.path.abspath(__file__)), "Data Nilai Rapor SMP Ke Provinsi.xlsx") 
print(dirname)
wb = xlrd.open_workbook(dirname) 
sh = wb.sheet_by_index(0)
dataRapor=[]
index=0
for i in range(sh.nrows):
    if i >= 4:
        value = sh.row_values(i)
        rapor=[]
        for j in range(5):
            rapor.append({
                "sms": (j+1),
                "score_religion": checkValidationScore(value[j + 6]),
                "score_pancasila": checkValidationScore(value[j + 11]),
                "score_bahasa": checkValidationScore(value[j + 16]),
                "score_math": checkValidationScore(value[j+21]),
                "score_physics":checkValidationScore( value[j+26]),
                "score_science": checkValidationScore(value[j+31]),
                "score_english": checkValidationScore(value[j+36]),
            })
        dataRapor.append({
            "un_year": str(value[1]).replace(".0", ""),
            "un_type": "reguler smp",
            "un": str(value[2]),
            "rapor": rapor
        })

for rapor in (dataRapor):
    mycoljunior.update_one({
        "$and": [
            {"un_year": rapor['un_year']},
            {"un": rapor['un']},
            {"un_type": rapor["un_type"]}
        ]
    }, {
        "$set": {
            "rapor": rapor["rapor"],
        }
    })
    print(rapor["un"])
  

# for fileName in files:
#     dirname = os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__)), dir), fileName)
#     print(dirname)