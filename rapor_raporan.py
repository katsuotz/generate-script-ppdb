import pymongo
import xlrd
import datetime
import json
import os

# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolJunior = mydb["ppdb_unjuniors"]

regis = mycolJunior.find_one({"un": "02-01-0001-0204-5","nisn":"0052333860"})
totalRapor = 0
for rapor in regis["rapor"]:
    if "score_bahasa" in rapor:
        totalRapor += float(rapor["score_bahasa"])
    if "score_english" in rapor:
        totalRapor += float(rapor["score_english"])
    if "score_math" in rapor:
        totalRapor += float(rapor["score_math"])
    if "score_physics" in rapor:
        totalRapor += float(rapor["score_physics"])
    if "score_religion" in rapor:
        totalRapor += float(rapor["score_religion"])
    if "score_pancasila" in rapor:
        totalRapor += float(rapor["score_pancasila"])
    if "score_science" in rapor:
        totalRapor += float(rapor["score_science"])

print(totalRapor)