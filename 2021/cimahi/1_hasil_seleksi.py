from dbfread import DBF
import json
import pymongo
import os
import datetime
from bson import json_util
from datetime import datetime


#myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
#mydb = myclient["ppdbCimahi"]
#myclient = pymongo.MongoClient("mongodb://ppdbCimahiDev:1@localhost:27017/ppdbCimahiDev")
#mydb = myclient["ppdbCimahiDev"]
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb_cimahi"]
myOptions = mydb['ppdb_options']
myRegis = mydb['ppdb_registrations']

time = datetime.now()

def checkValidationScore(score):
    score = str(score).strip()
    if score == '':
        return 0
    else:
        return float("{:.2f}".format(float(score)))

def reformatJarak(value):
    value = str(value).strip()
    if value == '':
        return 0
    else:
        return float("{:.3f}".format(float(value)))

# def reformatJarak(value):
#     value = str(value).strip()
#     if value == '':
#         return 0
#     else:
#         return float("{:.3f}".format(float(value)))

def getRegistrantByScoreJarak(option, limit):
    return list(
                myRegis.find({
                    "status": "approved",
                    "first_choice": option["_id"],
#                     "disqualified": {"$ne" : 1}
                }).sort([("distance1", pymongo.ASCENDING), ("createdAt", pymongo.ASCENDING)]).limit(limit)
           )

def getRegistrantByAge(option, limit):
    return list(
               myRegis.find({
                   "status": "approved",
                   "first_choice": option["_id"],
#                    "disqualified": {"$ne" : 1}
               }).sort([("birth_date", pymongo.ASCENDING),("distance1", pymongo.ASCENDING), ("createdAt", pymongo.ASCENDING)]).limit(limit)
          )

def main():
    print("Main")

    #level = ["prestasi", "prestasi-rapor", "ketm"]
    level = ["abk", "perpindahan", "zonasi"]

    myRegis.update_many({
        "type": {"$in": level}
        }, {
            "$set": {
                "accepted_option": None,
            }
        })

    #calculate rapor
    if "prestasi-rapor" in level:
        prestasiAk = myRegis.find({"level_pendaftaran":"prestasi-rapor"})
        for ak in prestasiAk:
           rapor = 0
           for r in ak["rapor"]:
               rapor+= checkValidationScore(r["score"])
           rapor = checkValidationScore(rapor)
           myRegis.update_one({"_id": ak["_id"]}, {"$set":{ "score": rapor }})

    #     calculate prestasi
    if "prestasi" in level:
        prestasi = myRegis.find({"level_pendaftaran":"prestasi-rapor"})
        for item in prestasi:
            if "prestasi" in item["special_requirment"]
               myRegis.update_one({"_id": item["_id"]}, {"$set":{ "score": item["special_requirment"]["prestasi"]["score"] }})

    opts = []

    options = list(myOptions.aggregate([
       {
           "$match": {
               "type": {"$in": level}
           },
       },
       {
           "$lookup": {
               "from": "ppdb_schools",
               "localField": "school_id",
               "foreignField": "_id",
               "as":"school"
           }
       },
       {
           "$unwind": "$school"
       }
   ]))

    for type in level:
        selected_options = []

        for option in options:
            if option["type"] == type:
                selected_options.append(option)

        for option in selected_options:
            regis = None
            if option["quota"] > 0:
                if option["school"]["level"] == 'elementary' and option["type"] != 'perpindahan' and option["type"] != 'zonasi':
                    regis = getRegistrantByAge(option, option["quota"])
                elif type != 'zonasi':
                    regis = getRegistrantByScoreJarak(option, option["quota"])

                if option["type"] == 'ketm':
                   r = myRegis.find({
                           "status": "approved",
                           "first_choice": option["_id"]
                       }).sort([("distance1", pymongo.ASCENDING), ("createdAt", pymongo.ASCENDING)]).limit(option["quota"])
                   regis = list(r)
                if option["type"] == 'prestasi' or option["type"] == "prestasi-rapor":
                   r = myRegis.find({
                           "status": "approved",
                           "first_choice": option["_id"],
                #            "disqualified": {"$ne" : 1}
                       }).sort([("score", pymongo.DESCENDING), ("createdAt", pymongo.ASCENDING)]).limit(option["quota"])
                   regis = list(r)

                total_registrant = 0

                if (regis is not None and len(regis) == 0):
                    total_registrant = 0
                elif (regis is not None and len(regis) > 0):
                    total_registrant = len(regis)

                if type != 'zonasi':
                    myOptions.update_one({"_id": option["_id"]}, {"$set": {"total_registrant": total_registrant}})
                    opts.append({
                        "_id": option["_id"],
                        "school_id": option["school_id"],
                        "type": option["type"],
                        "quota": option["quota"],
                        "registrant": total_registrant,
                    })

                #UNCOMMENT THIS LATER
                if (regis is not None and len(regis) > 0) or type == 'zonasi':
                    if type == 'zonasi':
                        quota = option["quota"]
                        registrant = 0
                        for opt in opts:
                            if opt["school_id"] == option["school_id"]:
                                quota += opt["quota"]
                                print(opt["type"] + " - quota :" + str(opt["quota"]) + " - registrant :" + str(opt["registrant"]))
                                registrant += opt["registrant"]

                        print(option["type"] + " - quota :" + str(option["quota"]))

                        limit = quota - registrant
                        if option["school"]["level"] == 'elementary':
                            regis = getRegistrantByAge(option, limit)
                        if option["school"]["level"] == 'junior':
                            regis = getRegistrantByScoreJarak(option, limit)
                            myOptions.update_one({"_id": option["_id"]}, {"$set": {"accepted_quota": limit}})
                        print("total_kuota_all:" + str(quota))
                        print("total_registrant_without_zonasi:" + str(registrant))
                        print("total_kuota_zonasi:" + str(limit) +", registrant terseleksi: " + str(len(regis)) + " - " + option["name"])

                    if len(regis) >= option["quota"]:
                       print("total_kuota:" + str(option["quota"]) +", registrant terseleksi: " + str(len(regis)) + " - " +option["name"]    )

                    for r in regis:
                        myRegis.update_one({"_id": r["_id"]}, {
                            "$set": {
                                "accepted_option": {
                                    "school_id":option["school_id"],
                                    "id":option["_id"],
                                    "registration_id": r["_id"],
                                    "type": option["type"],
                                    "name": option["name"],
                                    "createdAt": time,
                                    "updatedAt": time,
                                }
                            }})


if __name__ == '__main__':
    main()
