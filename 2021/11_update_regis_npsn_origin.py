import datetime
import json
import os
import pymongo
import datetime
from bson.objectid import ObjectId

start = datetime.datetime.now()
totalData = 0
getData = 0

# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = myclient["ppdb21"]
myRegis = mydb["ppdb_registrations"]
mydbref = myclient["ppdb21-reference"]
myJunior = mydbref["ref_junior_data"]

getJunior = myJunior.find({
    "luar_jabar": 1
})

for junior in getJunior:
    regis = myRegis.find_one({
        "junior_data": junior["_id"]
    })

    if regis is not None and "npsn_origin" in junior:
        print(regis["_id"])
        print(junior["npsn_origin"])
        myRegis.update_one({
            "_id": regis["_id"]
        }, {
            "$set": {
                "npsn_origin": junior["npsn_origin"]
            }
        })
        totalData+=1


end = datetime.datetime.now()
bench = end - start
print(str(totalData) + " updated in " + str(bench.seconds) + " seconds")