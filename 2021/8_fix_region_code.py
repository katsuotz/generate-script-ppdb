import os
import pymongo
import datetime
from dotenv import load_dotenv

start = datetime.datetime.now()
totalData = 0
getData = 0

load_dotenv()

mongodbUrl = os.getenv('DB_URL')
myclient = pymongo.MongoClient(mongodbUrl)
mydbref = myclient["ppdb-jabar-reference"]
myJunior = mydbref["ref_junior_data"]
myRegion = mydbref["ref_regions"]

getJunior = myJunior.find({
    "address_subdistrict": {"$exists": True, "$ne": ""},
})

for junior in getJunior:
    print(junior["_id"])

    region = myRegion.find_one({
        "province": junior["address_province"],
        "city": junior["address_city"],
        "district": junior["address_district"],
        "subdistrict": junior["address_subdistrict"],
    })

    getData+=1

    if region is not None and "code" in region:
        myJunior.update_one({
            "_id": junior["_id"]
        }, {
            "$set": {
                "region_code": region["code"],
            }
        })
        totalData+=1


end = datetime.datetime.now()
bench = end - start
print(str(getData) + " data fetched")
print(str(totalData) + " updated in " + str(bench.seconds) + " seconds")