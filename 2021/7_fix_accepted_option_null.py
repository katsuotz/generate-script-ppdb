import datetime
import json
import os
import pymongo
import datetime
from bson.objectid import ObjectId

start = datetime.datetime.now()
totalData = 0

# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb21"]
myRegis = mydb["ppdb_registrations"]
myOption = mydb["ppdb_options"]

getRegis = myRegis.find({
    "$or": [
        {
            "accepted_choice_id": {"$ne": None},
            "accepted_option": None,
        },
        {
            "accepted_choice_id": {"$ne": None},
            "accepted_option": {"$exists": True},
            "accepted_option.name": {"$exists": False}
        }
    ]
})

for regis in getRegis:
    option = myOption.find_one({
        "_id": regis["accepted_choice_id"]
    })

    major_id = None
    no_color_blind = None

    if "major_id" in option:
        major_id = option["major_id"]
    if "no_color_blind" in option:
        no_color_blind = option["no_color_blind"]

    myRegis.update_one({
        "_id": regis["_id"]
    }, {
        "$set": {
            "accepted_option": {
                "id": option["_id"],
                "name": option["name"],
                "type": option["type"],
                "major_id": major_id,
                "no_color_blind": no_color_blind,
                "_class": "com.filterisasi.filterisasi.model.PpdbRegistrationChoiceOption",
            },
        }
    })

    totalData+=1

#     else:
#         print(regis["_id"])

#         print("update " + str(regis["_id"]))


end = datetime.datetime.now()
bench = end - start
print(str(totalData) + " updated in " + str(bench.seconds) + " seconds")