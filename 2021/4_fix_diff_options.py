import datetime
import json
import os
import pymongo
import datetime

start = datetime.datetime.now()
totalData = 0

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb21"]
myRegis = mydb["ppdb_registrations"]

getRegis = myRegis.find()

for regis in getRegis:
    option_type = regis["option_type"]

    if "first_option" in regis:
        first_option = regis["first_option"]
        if first_option is not None and "type" in first_option and first_option["type"] != option_type:
            totalData+=1
            print(regis["_id"])
            myRegis.update_one({
                "_id": regis["_id"]
            }, {
                "$set": {
                    "status": "rejected",
                    "reason": "Perbaiki data pilihan"
                }
            })
            if option_type == "prestasi":
                myRegis.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": {
                        "score": 0,
                    }
                })
#             totalData+=1
#             myRegis.update_one({
#                 "_id": regis["_id"]
#             }, {
#                 "$unset": {
#                     "first_option": "",
#                     "first_school": "",
#                 }
#             })

    if "second_option" in regis:
        second_option = regis["second_option"]
        if second_option is not None and "type" in second_option and second_option["type"] != option_type and regis["second_choice_option"] != second_option["id"]:
            totalData+=1
            myRegis.update_one({
                "_id": regis["_id"]
            }, {
                "$unset": {
                    "second_option": "",
                    "second_school": "",
                }
            })
            if option_type == "prestasi":
                myRegis.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": {
                        "score": 0,
                    }
                })

    if "third_option" in regis:
        third_option = regis["third_option"]
        if third_option is not None and "type" in third_option and third_option["type"] != option_type and regis["third_choice_option"] != third_option["id"]:
            totalData+=1
            myRegis.update_one({
                "_id": regis["_id"]
            }, {
                "$unset": {
                    "third_option": "",
                    "third_school": "",
                }
            })
            if option_type == "prestasi":
                myRegis.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": {
                        "score": 0,
                    }
                })

end = datetime.datetime.now()
bench = end - start
print(str(totalData) + " data updated in " + str(bench.seconds) + " seconds")