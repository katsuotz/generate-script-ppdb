import datetime
import json
import os
import pymongo
import datetime
import sys

start = datetime.datetime.now()
totalData = 0

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb21"]
myUser = mydb["ppdb_users"]
mySchool = mydb["ppdb_schools"]

getUser = myUser.find({
    "role": "admin-juniorschool",
})

for user in getUser:
    school = mySchool.find_one({
        "_id": user["school"]
    })

    if school is not None and school["level"] == "slb":
        myUser.update_one({
            "_id": user["_id"]
        }, {
            "$set": {
                "role": "admin-highschool"
            }
        })

        totalData+=1


end = datetime.datetime.now()
bench = end - start
print(str(totalData) + " user data updated in " + str(bench.seconds) + " seconds")
