import datetime
import json
import os
import pymongo
import datetime

start = datetime.datetime.now()
totalData = 0

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb21"]
myRegis = mydb["ppdb_registrations"]

getRegis = myRegis.find({
    "option_type": {
        "$nin": [
            "jarak",
            "zonasi",
            "rapor",
            "prestasi-rapor",
            "rapor-unggulan",
        ]
    },
})

for regis in getRegis:
    if "special_requirment" not in regis or regis["option_type"] not in regis["special_requirment"]:
        print(regis["_id"])
        totalData+=1
        myRegis.update_one({
            "_id": regis["_id"]
        }, {
            "$set": {
                "status": "rejected",
                "reason": "Persyaratan Khusus Belum Lengkap"
            }
        })
#
#         print("update " + str(regis["_id"]))


getRegis = myRegis.find({
    "option_type": {
        "$in": [
            "rapor",
            "prestasi-rapor",
            "rapor-unggulan",
        ]
    },
    "score": 0
})

for regis in getRegis:
    if "special_requirment" not in regis or regis["option_type"] not in regis["special_requirment"]:
        print(regis["_id"])
        totalData+=1
        myRegis.update_one({
            "_id": regis["_id"]
        }, {
            "$set": {
                "status": "rejected",
                "reason": "Persyaratan Khusus Belum Lengkap"
            }
        })
#
#         print("update " + str(regis["_id"]))

getRegis = myRegis.find({
    "option_type": {
        "$in": [
            "prestasi",
        ]
    },
    "special_requirment.prestasi": {"$exists":False}
})


for regis in getRegis:
    print(regis["_id"])
    totalData+=1
    myRegis.update_one({
        "_id": regis["_id"]
    }, {
        "$set": {
            "status": "rejected",
            "reason": "Persyaratan Khusus Belum Lengkap"
        }
    })
#
#         print("update " + str(regis["_id"]))

getRegis = myRegis.find({
    "option_type": {
        "$in": [
            "prestasi",
        ]
    },
    "special_requirment.prestasi": {"$exists":True}
})

getRegis = myRegis.find({
    "option_type": {
        "$in": [
            "prestasi",
        ]
    },
    "special_requirment.prestasi": {"$exists":True}
})


for regis in getRegis:
    if "special_requirment" not in regis or regis["option_type"] not in regis["special_requirment"]:
        print(regis["_id"])
        totalData+=1
        myRegis.update_one({
            "_id": regis["_id"]
        }, {
            "$set": {
                "status": "rejected",
                "reason": "Persyaratan Khusus Belum Lengkap"
            }
        })
    elif "prestasi" in regis["special_requirment"]:
        if regis["special_requirment"]["prestasi"]["penyelenggara"] != "Hafidz Quran":
            if "score_sertifikat" not in regis["special_requirment"]["prestasi"] or regis["special_requirment"]["prestasi"]["score_sertifikat"] == 0:
                print(regis["_id"])
                totalData+=1
                regis["special_requirment"]["prestasi"]["nama_prestasi"] = ''
                regis["special_requirment"]["prestasi"]["peringkat"] = ''
                myRegis.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": {
                        "status": "rejected",
                        "reason": "Persyaratan Khusus Belum Lengkap",
                        "special_requirment": regis["special_requirment"],
                    }
                })


end = datetime.datetime.now()
bench = end - start
print(str(totalData) + " data updated in " + str(bench.seconds) + " seconds")