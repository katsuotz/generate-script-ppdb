import datetime
import json
import os
import pymongo
import datetime

start = datetime.datetime.now()
totalData = 0

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb21"]
myRegis = mydb["ppdb_registrations"]

getRegis = myRegis.find({
    "registration_choice": "mandiri",
    "school": {"$exists" : False}
})

for regis in getRegis:
    print(regis["_id"])
    totalData+=1
    myRegis.update_one({
        "_id": regis["_id"]
    }, {
        "$set": {
            "status": "rejected",
            "reason": "Biodata Belum Lengkap"
        }
    })

end = datetime.datetime.now()
bench = end - start
print(str(totalData) + " data updated in " + str(bench.seconds) + " seconds")