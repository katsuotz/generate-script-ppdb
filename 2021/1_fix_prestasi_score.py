import datetime
import json
import os
import pymongo
import datetime
from bson.objectid import ObjectId
from dotenv import load_dotenv

load_dotenv()

start = datetime.datetime.now()
totalData = 0

# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
mongodbUrl = os.getenv('DB_URL')
myclient = pymongo.MongoClient(mongodbUrl)
mydb = myclient["ppdb-jabar"]
myRegis = mydb["ppdb_registrations"]
mySchool = mydb["ppdb_schools"]

score = {
  'Kemdikbud / Kemenag': {
    'Internasional': {
      '1': {
        'Tunggal': 455,
        'Beregu': 445,
      },
      '2': {
        'Tunggal': 440,
        'Beregu': 430,
      },
      '3': {
        'Tunggal': 425,
        'Beregu': 415,
      },
    },
    'Asia': {
      '1': {
        'Tunggal': 410,
        'Beregu': 400,
      },
      '2': {
        'Tunggal': 395,
        'Beregu': 385,
      },
      '3': {
        'Tunggal': 380,
        'Beregu': 370,
      },
    },
    'Nasional': {
      '1': {
        'Tunggal': 365,
        'Beregu': 355,
      },
      '2': {
        'Tunggal': 350,
        'Beregu': 340,
      },
      '3': {
        'Tunggal': 335,
        'Beregu': 325,
      },
      'Harapan': {
        'Tunggal': 320,
        'Beregu': 310,
      },
    },
    'Provinsi': {
      '1': {
        'Tunggal': 305,
        'Beregu': 295,
      },
      '2': {
        'Tunggal': 290,
        'Beregu': 280,
      },
      '3': {
        'Tunggal': 275,
        'Beregu': 265,
      },
      'Harapan': {
        'Tunggal': 260,
        'Beregu': 250,
      },
    },
    'Kota': {
      '1': {
        'Tunggal': 245,
        'Beregu': 235,
      },
      '2': {
        'Tunggal': 230,
        'Beregu': 220,
      },
      '3': {
        'Tunggal': 215,
        'Beregu': 205,
      },
      'Harapan': {
        'Tunggal': 200,
        'Beregu': 190,
      },
    },
  },
  'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi': {
    'Internasional': {
      '1': {
        'Tunggal': 400,
        'Beregu': 390,
      },
      '2': {
        'Tunggal': 385,
        'Beregu': 375,
      },
      '3': {
        'Tunggal': 370,
        'Beregu': 360,
      },
    },
    'Asia': {
      '1': {
        'Tunggal': 355,
        'Beregu': 345,
      },
      '2': {
        'Tunggal': 340,
        'Beregu': 330,
      },
      '3': {
        'Tunggal': 325,
        'Beregu': 315,
      },
    },
    'Nasional': {
      '1': {
        'Tunggal': 310,
        'Beregu': 300,
      },
      '2': {
        'Tunggal': 295,
        'Beregu': 285,
      },
      '3': {
        'Tunggal': 280,
        'Beregu': 275,
      },
      'Harapan': {
        'Tunggal': 265,
        'Beregu': 255,
      },
    },
    'Provinsi': {
      '1': {
        'Tunggal': 250,
        'Beregu': 240,
      },
      '2': {
        'Tunggal': 235,
        'Beregu': 225,
      },
      '3': {
        'Tunggal': 220,
        'Beregu': 210,
      },
      'Harapan': {
        'Tunggal': 205,
        'Beregu': 195,
      },
    },
    'Kota': {
      '1': {
        'Tunggal': 190,
        'Beregu': 180,
      },
      '2': {
        'Tunggal': 175,
        'Beregu': 165,
      },
      '3': {
        'Tunggal': 160,
        'Beregu': 150,
      },
      'Harapan': {
        'Tunggal': 145,
        'Beregu': 135,
      },
    },
  },
  'Paskibra diselenggarakan Kemdikbud/Kemenpora/Purna Paskibraka Indonesia': {
    'Nasional': {
      '1': {
        'Tunggal': 365,
        'Beregu': 355,
      },
      '2': {
        'Tunggal': 350,
        'Beregu': 340,
      },
      '3': {
        'Tunggal': 335,
        'Beregu': 325,
      },
      'Harapan': {
        'Tunggal': 320,
        'Beregu': 310,
      },
    },
    'Provinsi': {
      '1': {
        'Tunggal': 305,
        'Beregu': 295,
      },
      '2': {
        'Tunggal': 290,
        'Beregu': 280,
      },
      '3': {
        'Tunggal': 275,
        'Beregu': 265,
      },
      'Harapan': {
        'Tunggal': 260,
        'Beregu': 250,
      },
    },
    'Kota': {
      '1': {
        'Tunggal': 245,
        'Beregu': 235,
      },
      '2': {
        'Tunggal': 235,
        'Beregu': 225,
      },
      '3': {
        'Tunggal': 225,
        'Beregu': 215,
      },
      'Harapan': {
        'Tunggal': 215,
        'Beregu': 205,
      },
    },
  },
  'Paskibra diselenggarakan diluar Kemdikbud/Kemenpora/Purna Paskibraka Indonesia': {
    'Nasional': {
      '1': {
        'Tunggal': 150,
        'Beregu': 140,
      },
      '2': {
        'Tunggal': 140,
        'Beregu': 130,
      },
      '3': {
        'Tunggal': 130,
        'Beregu': 120,
      },
      'Harapan': {
        'Tunggal': 120,
        'Beregu': 110,
      },
    },
    'Provinsi': {
      '1': {
        'Tunggal': 110,
        'Beregu': 100,
      },
      '2': {
        'Tunggal': 100,
        'Beregu': 90,
      },
      '3': {
        'Tunggal': 90,
        'Beregu': 80,
      },
      'Harapan': {
        'Tunggal': 80,
        'Beregu': 70,
      },
    },
    'Kota': {
      '1': {
        'Tunggal': 70,
        'Beregu': 60,
      },
      '2': {
        'Tunggal': 60,
        'Beregu': 50,
      },
      '3': {
        'Tunggal': 50,
        'Beregu': 40,
      },
      'Harapan': {
        'Tunggal': 40,
        'Beregu': 30,
      },
    },
  },
}

def getScore (prestasi):
    try:
        penyelenggara = prestasi["penyelenggara"]
        tingkat = prestasi['tingkat']
        kategori = prestasi['kategori']
        peringkat = prestasi['peringkat']

        if penyelenggara == 'West Java Leader’s Reading Challenge (WJLRC)':
            penyelenggara = 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi'
            peringkat = 1

        result = int(score[penyelenggara][tingkat][peringkat][kategori])

        return result
    except:
        return 0

def getScoreHafidz (prestasi):
    score = 0
    value = int(prestasi["jumlah_juz"])

    if (value >= 11 and value <= 30):
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Internasional',
            "peringkat": '1',
            "kategori": 'Tunggal'
        })
    elif (value >= 7 and value <= 10):
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Nasional',
            "peringkat": '1',
            "kategori": 'Tunggal'
        })
    elif (value >= 4 and value <= 6):
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Provinsi',
            "peringkat": '1',
            "kategori": 'Tunggal'
        })
    elif (value == 3):
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Kota',
            "peringkat": '1',
            "kategori": 'Tunggal'
        })

    if score > 0:
        prestasi["score_sertifikat"] = score

    return score


def getScorePramuka(prestasi):
    score = 0
    if prestasi["nama_prestasi"] == 'Piagam Jambore Dunia':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Internasional',
            "peringkat": '1',
            "kategori": 'Tunggal'
        })
    if prestasi["nama_prestasi"] == 'Piagam Jambore Internasional/Regional':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Internasional',
            "peringkat": '2',
            "kategori": 'Tunggal'
        })
    if prestasi["nama_prestasi"] == 'Piagam Pramuka Teladan':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Internasional',
            "peringkat": '3',
            "kategori": 'Tunggal'
        })
    if prestasi["nama_prestasi"] == 'Piagam Pramuka Garuda':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Nasional',
            "peringkat": '1',
            "kategori": 'Tunggal'
        })
    if prestasi["nama_prestasi"] == 'Piagam Jambore Nasional':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Nasional',
            "peringkat": '2',
            "kategori": 'Tunggal'
        })
    if prestasi["nama_prestasi"] == 'Juara 1 Lomba Tingkat V (Nasional)':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Nasional',
            "peringkat": '1',
            "kategori": 'Beregu'
        })
    if prestasi["nama_prestasi"] == 'Juara 2 Lomba Tingkat V (Nasional)':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Nasional',
            "peringkat": '2',
            "kategori": 'Beregu'
        })
    if prestasi["nama_prestasi"] == 'Juara 3 Lomba Tingkat V (Nasional)':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Nasional',
            "peringkat": '3',
            "kategori": 'Beregu'
        })
    if prestasi["nama_prestasi"] == 'Piagam Jambore Daerah Jawa Barat (Provinsi)':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Provinsi',
            "peringkat": '1',
            "kategori": 'Tunggal'
        })
    if prestasi["nama_prestasi"] == 'Kegiatan Kwarda Jawa Barat (Provinsi)':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Provinsi',
            "peringkat": '2',
            "kategori": 'Tunggal'
        })
    if prestasi["nama_prestasi"] == 'Juara 1 Lomba Tingkat IV (Provinsi)':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Provinsi',
            "peringkat": '1',
            "kategori": 'Beregu'
        })
    if prestasi["nama_prestasi"] == 'Juara 2 Lomba Tingkat IV (Provinsi)':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Provinsi',
            "peringkat": '2',
            "kategori": 'Beregu'
        })
    if prestasi["nama_prestasi"] == 'Juara 3 Lomba Tingkat IV (Provinsi)':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Provinsi',
            "peringkat": '3',
            "kategori": 'Beregu'
        })
    if prestasi["nama_prestasi"] == 'Piagam Jambore Cabang Tingkat Kota/Kabupaten':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Kota',
            "peringkat": '1',
            "kategori": 'Tunggal'
        })
    if prestasi["nama_prestasi"] == 'Kegiatan Kwartir Cabang Tingkat Kota/Kabupaten':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Kota',
            "peringkat": '2',
            "kategori": 'Tunggal'
        })
    if prestasi["nama_prestasi"] == 'Juara 1 Lomba Tingkat III (Kota/Kabupaten)':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Kota',
            "peringkat": '1',
            "kategori": 'Beregu'
        })
    if prestasi["nama_prestasi"] == 'Juara 2 Lomba Tingkat III (Kota/Kabupaten)':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Kota',
            "peringkat": '2',
            "kategori": 'Beregu'
        })
    if prestasi["nama_prestasi"] == 'Juara 3 Lomba Tingkat III (Kota/Kabupaten)':
        score = getScore({
            "penyelenggara": 'Diluar Kemdikbud / Kemenag / Dari Induk Organisasi',
            "tingkat": 'Kota',
            "peringkat": '3',
            "kategori": 'Beregu'
        })

    return score

getRegis = myRegis.find({
    "option_type": "prestasi",
    "special_requirment.prestasi": {"$exists":True},
})

for regis in getRegis:
    if "prestasi" in regis["special_requirment"]:
        if "penyelenggara" in regis["special_requirment"]["prestasi"] and (regis["special_requirment"]["prestasi"]["penyelenggara"] == "Hafidz Quran" or regis["special_requirment"]["prestasi"]["penyelenggara"] == "Pramuka"):
            if regis["special_requirment"]["prestasi"]["penyelenggara"] == "Hafidz Quran":
                regis["special_requirment"]["prestasi"]["jumlah_juz"] = int(regis["special_requirment"]["prestasi"]["jumlah_juz"])
                regis["special_requirment"]["prestasi"]["score_sertifikat"] = getScoreHafidz(regis["special_requirment"]["prestasi"])
            elif regis["special_requirment"]["prestasi"]["penyelenggara"] == "Pramuka":
                regis["special_requirment"]["prestasi"]["score_sertifikat"] = getScorePramuka(regis["special_requirment"]["prestasi"])
        elif "pelaksanaan" in regis["special_requirment"]["prestasi"]:
            if regis["special_requirment"]["prestasi"]["pelaksanaan"] == "Tidak Berjenjang":
                regis["special_requirment"]["prestasi"]["score_sertifikat"] = getScore(regis["special_requirment"]["prestasi"])
            elif regis["special_requirment"]["prestasi"]["pelaksanaan"] == "Berjenjang":
                penyelenggara = regis["special_requirment"]["prestasi"]["penyelenggara"]
                kategori = regis["special_requirment"]["prestasi"]["kategori"]

                tingkatKey = 0
                score_sertifikat = 0
                if "berjenjang" in regis["special_requirment"]["prestasi"]:
                    for item in regis["special_requirment"]["prestasi"]["berjenjang"]:
                        if "peringkat" in item:
                            peringkat = item["peringkat"]
                            tingkat = ''
                            if tingkatKey == 0:
                                tingkat = 'Kota'
                            elif tingkatKey == 1:
                                tingkat = 'Provinsi'
                            elif tingkatKey == 2:
                                tingkat = 'Nasional'
                            elif tingkatKey == 3:
                                tingkat = 'Interasional'
                            regis["special_requirment"]["prestasi"]["berjenjang"][tingkatKey]["score_sertifikat"] = getScore({
                                                                                                                          "penyelenggara": penyelenggara,
                                                                                                                          "tingkat": tingkat,
                                                                                                                          "peringkat": peringkat,
                                                                                                                          "kategori": kategori
                                                                                                                        })
                            score_sertifikat += regis["special_requirment"]["prestasi"]["berjenjang"][tingkatKey]["score_sertifikat"]
                            tingkatKey+=1
                    regis["special_requirment"]["prestasi"]["score_sertifikat"] = score_sertifikat

        totalData+=1
        score_kejuaraan = 0
        score_ujikom = 0
        score_akhir = 0

        if "score_sertifikat" in regis["special_requirment"]["prestasi"] and regis["special_requirment"]["prestasi"]["score_sertifikat"] is not None:
            score_kejuaraan = regis["special_requirment"]["prestasi"]["score_sertifikat"]

        selectedSchool = mySchool.find_one({
            "_id": regis["first_school_id"]
        })

        if "score_ujikom" in regis and regis["score_ujikom"] is not None:
            score_ujikom = regis["score_ujikom"]

        update = {
            "special_requirment": regis["special_requirment"],
        }

        if "ujikom_prestasi" in selectedSchool and selectedSchool["ujikom_prestasi"] == 0:
            update["score"] = score_kejuaraan
            update["score_kejuaraan"] = score_kejuaraan
            update["score_ujikom"] = score_kejuaraan
        else:
            if score_ujikom != 0 and score_kejuaraan != 0:
                score_akhir = ((score_kejuaraan * 30 / 100) + (score_ujikom * 70 / 100))
                update["score"] = score_akhir
                update["score_kejuaraan"] = score_kejuaraan
            else:
                update["score"] = 0
                update["score_kejuaraan"] = 0

        if regis["_id"] == "647ecdd19c2067306d73ca5c":
            print(regis["_id"])
            print(update)

        # if "score_sertifikat" not in regis["special_requirment"]["prestasi"] or regis["special_requirment"]["prestasi"]["score_sertifikat"] == 0:
        #     update["status"] = "rejected"
        #     update["reason"] = "Mohon perbaiki data prestasi anda"

        myRegis.update_one({
            "_id": regis["_id"]
        }, {
            "$set": update,
        })

end = datetime.datetime.now()
bench = end - start
print(str(totalData) + " updated in " + str(bench.seconds) + " seconds")
