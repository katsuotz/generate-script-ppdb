import pymongo
import json
import os
import datetime
import math
from dotenv import load_dotenv
load_dotenv()

# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
mongodbUrl = os.getenv('DB_URL')
myclient = pymongo.MongoClient(mongodbUrl)
mydb = myclient["ppdb-jabar"]
mydbref = myclient["ppdb-jabar-reference"]
mySchool = mydb["ppdb_schools"]
myRegis = mydb["ppdb_registrations"]
myJunior = mydbref["ref_junior_data"]

start = datetime.datetime.now()
totalData = 0

regisArray = myRegis.find({
    "option_type": {
        "$in": ['rapor', 'prestasi-rapor', 'rapor-unggulan'],
    },
    "rapor": {
        "$size": 0,
    },
    "status": {
        "$ne": "draft"
    },
})

for regis in regisArray:
    myRegis.update_one({
        "_id": regis["_id"]
    }, {
        "$set": {
            "status": "rejected",
            "reason": "Mohon perbaiki data rapor anda",
        }
    })
    print(regis["_id"])

    totalData+=1

end = datetime.datetime.now()
bench = end - start
print(str(totalData) + " updated in " + str(bench.seconds) + " seconds")
# regisArray = myRegis.find({"level_pendaftaran": "ketm", "document.ketm.domisili": {"$exists": True}})

# print("KETM")

# for regis in regisArray:
#     latDomisili = regis["document"]["ketm"]["domisili"]["coordinate_lat"]
#     lonDomisili = regis["document"]["ketm"]["domisili"]["coordinate_long"]
#     print(regis["code"])
#     if (latDomisili != "undefined") and (lonDomisili != "undefined"):
#         if "first_choice_school" in regis:
#             school = mySchool.find_one({"_id": regis["first_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     myRegis.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_domisili_1": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })

#         if "second_choice_school" in regis:
#             school = mySchool.find_one({"_id": regis["second_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     myRegis.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_domisili_2": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })


# regisArray = myRegis.find({"level_pendaftaran": "perpindahan", "document.perpindahan.perpindahan": {"$exists": True}})

# print("Perpindahan")

# for regis in regisArray:
#     latDomisili = regis["document"]["perpindahan"]["perpindahan"]["coordinate_lat"]
#     lonDomisili = regis["document"]["perpindahan"]["perpindahan"]["coordinate_long"]
#     print(regis["code"])
#     if (latDomisili != "undefined") and (lonDomisili != "undefined"):
#         if "first_choice_school" in regis:
#             school = mySchool.find_one({"_id": regis["first_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     myRegis.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_perpindahan_1": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })

#         if "second_choice_school" in regis:
#             school = mySchool.find_one({"_id": regis["second_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     myRegis.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_perpindahan_2": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })


# # print(calculate(-6.591476,106.783688,-6.5856444,106.7838887))