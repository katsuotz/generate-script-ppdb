import pymongo
import json
import os
import datetime
import math
from geopy.distance import great_circle
from dotenv import load_dotenv

load_dotenv()

mongodbUrl = os.getenv('DB_URL')
myclient = pymongo.MongoClient(mongodbUrl)
mydb = myclient["ppdb-jabar"]
mydbref = myclient["ppdb-jabar-reference"]
mySchool = mydb["ppdb_schools"]
myRegis = mydb["ppdb_registrations"]
myJunior = mydbref["ref_junior_data"]

start = datetime.datetime.now()
totalData = 0

def calculate(lat1, lon1, lat2, lon2) :
    if (lat1 == lat2) and (lon1 == lon2) :
        return 0
    else :
        radlat1 = math.pi * lat1 / 180
        radlat2 = math.pi * lat2 / 180
        theta = lon1 - lon2
        radtheta = math.pi * theta / 180
        dist = math.sin(radlat1) * math.sin(radlat2) + math.cos(radlat1) * math.cos(radlat2) * math.cos(radtheta)
        if dist > 1:
            dist = 1
        dist=math.acos(dist)
        dist=dist*180/math.pi
        dist=dist*60*1.1515
        dist=dist* 1.609344 * 1000
        return float("{:.3f}".format(dist))

def calculatev2(lat1, lon1, lat2, lon2) :
    if (lat1 == lat2) and (lon1 == lon2) :
            return 0
    else :
       return float("{:.3f}".format(great_circle((lat1,lon1), (lat2, lon2)).meters))

def toFloat(num):
    return float(str(num).strip().replace(",", ".").replace("°", ".").replace(" ", "."))

regisArray = myRegis.find({
    "option_type": {
        "$in": ["abk", "ketm", "kondisi-tertentu", "perpindahan", "zonasi", "jarak"],
    },
    "status": {
        "$nin": ["rejected", "draft"],
    },
})

for regis in regisArray:
    junior = myJunior.find_one({
        "_id": regis["junior_data_id"]
    })

    if junior is not None and "coordinate_lat" in junior and "coordinate_lng" in junior:
        oldLat = regis["coordinate_lat"]
        oldLon = regis["coordinate_lng"]
        latDomisili = junior["coordinate_lat"]
        lonDomisili = junior["coordinate_lng"]
        latSchool = None
        lonSchool = None

        if regis["option_type"] == "perpindahan" and regis["special_requirment"]["perpindahan"]["option_type"] == "perpindahan":
            oldLat = regis["special_requirment"]["perpindahan"]["coordinate_lat"]
            oldLon = regis["special_requirment"]["perpindahan"]["coordinate_lng"]
            latDomisili = junior["special_requirment"]["perpindahan"]["coordinate_lat"]
            lonDomisili = junior["special_requirment"]["perpindahan"]["coordinate_lng"]

        if oldLat != latDomisili or oldLon != lonDomisili:
            distance1 = 99999
            distance2 = 99999
            distance3 = 99999
            first_school_update = regis["first_school"]
            second_school_update = regis["second_school"]
            third_school_update = regis["third_school"]

            if "first_school_id" in regis:
                school = mySchool.find_one({"_id": regis["first_school_id"]})
                if school is not None:
                    first_school = regis["first_school"]
                    latSchool=school["coordinate_lat"]
                    lonSchool=school["coordinate_lng"]
                    if (latSchool != "") and (lonSchool != "") and (latSchool is not None) and (lonSchool is not None) and (latDomisili != "") and (lonDomisili != "") and (latDomisili is not None) and (lonDomisili is not None):
                        first_school["coordinate_lat"] = latSchool
                        first_school["coordinate_lng"] = lonSchool
                        first_school_update = first_school
                        distance1 = calculate(
                            toFloat(latDomisili),
                            toFloat(lonDomisili),
                            toFloat(latSchool),
                            toFloat(lonSchool))

            if "second_school" in regis and regis["second_school"] is not None:
                print("check second school")
                school = mySchool.find_one({"_id": regis["second_school"]["id"]})
                print(school)
                latSchool = regis["second_school"]["coordinate_lat"]
                lonSchool = regis["second_school"]["coordinate_lng"]
                if school is not None:
                    second_school = regis["second_school"]
                    latSchool=school["coordinate_lat"]
                    lonSchool=school["coordinate_lng"]
                    print(latSchool)
                    print(lonSchool)
                    if (latSchool != "") and (lonSchool != "") and (latSchool is not None) and (lonSchool is not None) and (latDomisili != "") and (lonDomisili != "") and (latDomisili is not None) and (lonDomisili is not None):
                        second_school["coordinate_lat"] = latSchool
                        second_school["coordinate_lng"] = lonSchool
                        print("update second school")
                        second_school_update = second_school

                if (latSchool != "") and (lonSchool != "") and (latSchool is not None) and (lonSchool is not None) and (latDomisili != "") and (lonDomisili != "") and (latDomisili is not None) and (lonDomisili is not None):
                    distance2 = calculate(
                        toFloat(latDomisili),
                        toFloat(lonDomisili),
                        toFloat(latSchool),
                        toFloat(lonSchool))

            if "third_school" in regis and regis["third_school"] is not None:
                school = mySchool.find_one({"_id": regis["third_school"]["id"]})
                latSchool = regis["third_school"]["coordinate_lat"]
                lonSchool = regis["third_school"]["coordinate_lng"]
                if school is not None:
                    third_school = regis["third_school"]
                    latSchool=school["coordinate_lat"]
                    lonSchool=school["coordinate_lng"]
                    if (latSchool != "") and (lonSchool != "") and (latSchool is not None) and (lonSchool is not None) and (latDomisili != "") and (lonDomisili != "") and (latDomisili is not None) and (lonDomisili is not None):
                        third_school["coordinate_lat"] = latSchool
                        third_school["coordinate_lng"] = lonSchool
                        third_school_update = second_school

                if (latSchool != "") and (lonSchool != "") and (latSchool is not None) and (lonSchool is not None) and (latDomisili != "") and (lonDomisili != "") and (latDomisili is not None) and (lonDomisili is not None):
                    distance3 = calculate(
                        toFloat(latDomisili),
                        toFloat(lonDomisili),
                        toFloat(latSchool),
                        toFloat(lonSchool))

            if regis["option_type"] == "perpindahan" and regis["special_requirment"]["perpindahan"]["option_type"] == "perpindahan":
                regis["special_requirment"]["perpindahan"]["coordinate_lat"] = junior["special_requirment"]["perpindahan"]["coordinate_lat"]
                regis["special_requirment"]["perpindahan"]["coordinate_lng"] = junior["special_requirment"]["perpindahan"]["coordinate_lng"]

                update = {
                    "special_requirment": regis["special_requirment"],
                    "distance1": distance1,
                    "distance2": distance2,
                    "distance3": distance3,
                }

                if first_school_update is not None:
                    update["first_school"] = first_school_update
                if second_school_update is not None:
                    update["second_school"] = second_school_update
                if third_school_update is not None:
                    update["third_school"] = third_school_update

                myRegis.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": update
                })
            else:
                update = {
                    "coordinate_lat": latDomisili,
                    "coordinate_lng": lonDomisili,
                    "distance1": distance1,
                    "distance2": distance2,
                    "distance3": distance3,
                }

                if first_school_update is not None:
                    update["first_school"] = first_school_update
                if second_school_update is not None:
                    update["second_school"] = second_school_update
                if third_school_update is not None:
                    update["third_school"] = third_school_update

                myRegis.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": update
                })

            totalData+=1
        # print(regis["_id"])


end = datetime.datetime.now()
bench = end - start
print(str(totalData) + " updated in " + str(bench.seconds) + " seconds")
# regisArray = myRegis.find({"level_pendaftaran": "ketm", "document.ketm.domisili": {"$exists": True}})

# print("KETM")

# for regis in regisArray:
#     latDomisili = regis["document"]["ketm"]["domisili"]["coordinate_lat"]
#     lonDomisili = regis["document"]["ketm"]["domisili"]["coordinate_long"]
#     print(regis["code"])
#     if (latDomisili != "undefined") and (lonDomisili != "undefined"):
#         if "first_choice_school" in regis:
#             school = mySchool.find_one({"_id": regis["first_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     myRegis.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_domisili_1": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })

#         if "second_choice_school" in regis:
#             school = mySchool.find_one({"_id": regis["second_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     myRegis.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_domisili_2": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })


# regisArray = myRegis.find({"level_pendaftaran": "perpindahan", "document.perpindahan.perpindahan": {"$exists": True}})

# print("Perpindahan")

# for regis in regisArray:
#     latDomisili = regis["document"]["perpindahan"]["perpindahan"]["coordinate_lat"]
#     lonDomisili = regis["document"]["perpindahan"]["perpindahan"]["coordinate_long"]
#     print(regis["code"])
#     if (latDomisili != "undefined") and (lonDomisili != "undefined"):
#         if "first_choice_school" in regis:
#             school = mySchool.find_one({"_id": regis["first_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     myRegis.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_perpindahan_1": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })

#         if "second_choice_school" in regis:
#             school = mySchool.find_one({"_id": regis["second_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     myRegis.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_perpindahan_2": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })


# # print(calculate(-6.591476,106.783688,-6.5856444,106.7838887))
