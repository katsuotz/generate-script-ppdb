import pymongo
import json
import os
import datetime
import math
from dotenv import load_dotenv

load_dotenv()

# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
mongodbUrl = os.getenv('DB_URL')
myclient = pymongo.MongoClient(mongodbUrl)
mydb = myclient["ppdb-jabar"]
mydbref = myclient["ppdb-jabar-reference"]
mySchool = mydb["ppdb_schools"]
myRegis = mydb["ppdb_registrations"]
myJunior = mydbref["ref_junior_data"]

start = datetime.datetime.now()
totalData = 0
totalReject = 0

regisArray = myRegis.find({
    "$or": [
        {"coordinate_lat": "0.000000000000"},
        {"coordinate.lng": "0.000000000000"},
        {"coordinate_lat": ""},
        {"coordinate.lng": ""},
        {"coordinate_lat": None},
        {"coordinate_lng": None},
    ],
    "option_type": {
        "$in": ["abk", "ketm", "kondisi-tertentu", "perpindahan", "zonasi", "jarak"]
    }
})

for regis in regisArray:
    junior = myJunior.find_one({
        "_id": regis["junior_data_id"]
    })

    reject = False

    if junior is not None:
        if "coordinate_lat" in junior and junior["coordinate_lat"] is not None and "coordinate_lng" in junior and junior["coordinate_lng"] is not None:
            myRegis.update_one({
                "_id": regis["_id"]
            }, {
                "$set": {
                    "coordinate_lat": junior["coordinate_lat"],
                    "coordinate_lng": junior["coordinate_lng"],
                }
            })

            totalData += 1
            print("update")
            print(regis["_id"])
        else:
            reject = True
    else:
        reject = True

    if reject == True:
        if regis["option_type"] == "perpindahan" and regis["special_requirment"]["perpindahan"]["option_type"] == "perpindahan":
            reject = False

    if reject == True:
        myRegis.update_one({
            "_id": regis["_id"]
        }, {
            "$set": {
                "status": "rejected",
                "reason": "Mohon perbaiki koordinat anda di Persyaratan Khusus",
            }
        })
        print("reject")
        print(regis["_id"])
        totalReject += 1

end = datetime.datetime.now()
bench = end - start
print(str(totalData) + " updated in total " + str(bench.seconds) + " seconds")
print(str(totalReject) + " rejected in total " + str(bench.seconds) + " seconds")
# regisArray = myRegis.find({"level_pendaftaran": "ketm", "document.ketm.domisili": {"$exists": True}})

# print("KETM")

# for regis in regisArray:
#     latDomisili = regis["document"]["ketm"]["domisili"]["coordinate_lat"]
#     lonDomisili = regis["document"]["ketm"]["domisili"]["coordinate_long"]
#     print(regis["code"])
#     if (latDomisili != "undefined") and (lonDomisili != "undefined"):
#         if "first_choice_school" in regis:
#             school = mySchool.find_one({"_id": regis["first_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     myRegis.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_domisili_1": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })

#         if "second_choice_school" in regis:
#             school = mySchool.find_one({"_id": regis["second_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     myRegis.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_domisili_2": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })


# regisArray = myRegis.find({"level_pendaftaran": "perpindahan", "document.perpindahan.perpindahan": {"$exists": True}})

# print("Perpindahan")

# for regis in regisArray:
#     latDomisili = regis["document"]["perpindahan"]["perpindahan"]["coordinate_lat"]
#     lonDomisili = regis["document"]["perpindahan"]["perpindahan"]["coordinate_long"]
#     print(regis["code"])
#     if (latDomisili != "undefined") and (lonDomisili != "undefined"):
#         if "first_choice_school" in regis:
#             school = mySchool.find_one({"_id": regis["first_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     myRegis.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_perpindahan_1": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })

#         if "second_choice_school" in regis:
#             school = mySchool.find_one({"_id": regis["second_choice_school"]})
#             if school is not None:
#                 latSchool=school["coordinate_lat"]
#                 lonSchool=school["coordinate_lng"]
#                 if (latSchool != "") and (lonSchool != ""):
#                     myRegis.update_one({
#                         "_id": regis["_id"]
#                     }, {
#                         "$set": {
#                             "score_jarak_perpindahan_2": calculatev2( float(latDomisili),float(lonDomisili),float(latSchool),float(lonSchool))
#                         }
#                     })


# # print(calculate(-6.591476,106.783688,-6.5856444,106.7838887))
