import datetime
import json
import os
import pymongo
import datetime
from bson.objectid import ObjectId

start = datetime.datetime.now()
totalData = 0
getData = 0

# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = myclient["ppdb21"]
myRegis = mydb["ppdb_registrations"]

getRegis = myRegis.find({
    "option_type": {
        "$in": ["zonasi", "rapor"],
    },
    "created_at": "$updated_at"
})

for regis in getRegis:
    myRegis.update_one({
        "_id": regis["_id"]
    }, {
        "$set": {
            "created_at": regis["updated_at"]
        }
    })
    totalData+=1

getRegis = myRegis.find({
    "option_type": {
        "$in": ["zonasi", "rapor"],
    },
    "created_at": "$verified_at"
})

for regis in getRegis:
    myRegis.update_one({
        "_id": regis["_id"]
    }, {
        "$set": {
            "created_at": regis["verified_at"]
        }
    })
    totalData+=1


end = datetime.datetime.now()
bench = end - start
print(str(totalData) + " updated in " + str(bench.seconds) + " seconds")