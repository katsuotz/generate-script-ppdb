import datetime
import json
import os
import pymongo
import datetime
import sys

start = datetime.datetime.now()
totalDataRegis = 0
totalDataJunior = 0

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb21"]
myRegis = mydb["ppdb_registrations"]

mydbref = myclient["ppdb21-reference"]
myJunior = mydbref["ref_junior_data"]

getRegis = myRegis.find({
    "general_requirment": {"$exists":True}
})

for regis in getRegis:
    if "data_disduk" in regis["general_requirment"] and regis["general_requirment"]["data_disduk"] is not None:
        nik = str(int(regis["general_requirment"]["data_disduk"]["NIK"]))
        no_kk = str(int(regis["general_requirment"]["data_disduk"]["NO_KK"]))
        regis["general_requirment"]["data_disduk"]["NIK"] = nik
        regis["general_requirment"]["data_disduk"]["NO_KK"] = no_kk

        myRegis.update_one({
            "_id": regis["_id"]
        }, {
            "$set": {
                "general_requirment": regis["general_requirment"]
            }
        })

        totalDataRegis+=1

getJunior = myJunior.find({
    "general_requirment": {"$exists":True}
})

for junior in getJunior:
    if "data_disduk" in junior["general_requirment"] and junior["general_requirment"]["data_disduk"] is not None:
        nik = str(int(junior["general_requirment"]["data_disduk"]["NIK"]))
        no_kk = str(int(junior["general_requirment"]["data_disduk"]["NO_KK"]))
        junior["general_requirment"]["data_disduk"]["NIK"] = nik
        junior["general_requirment"]["data_disduk"]["NO_KK"] = no_kk

        myJunior.update_one({
            "_id": junior["_id"]
        }, {
            "$set": {
                "general_requirment": junior["general_requirment"]
            }
        })

        totalDataJunior+=1


end = datetime.datetime.now()
bench = end - start
print(str(totalDataRegis) + " regis data updated in " + str(bench.seconds) + " seconds")
print(str(totalDataJunior) + " junior data updated in " + str(bench.seconds) + " seconds")