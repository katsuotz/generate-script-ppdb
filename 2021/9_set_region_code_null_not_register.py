import datetime
import json
import os
import pymongo
import datetime
from bson.objectid import ObjectId

start = datetime.datetime.now()
totalData = 0
getData = 0

# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = myclient["ppdb21"]
mydbref = myclient["ppdb21-reference"]
myRegis = mydb["ppdb_registrations"]
myJunior = mydbref["ref_junior_data"]
myRegion = mydbref["ref_regions"]

getJunior = myJunior.find({
    "address_subdistrict": {"$exists": True, "$ne": ""},
    "region_code": None
})

for junior in getJunior:
    getData+=1

    regis = myRegis.find_one({
        "junior_data": junior["_id"],
    })

    if regis is None:
        myJunior.update_one({
            "_id": junior["_id"]
        }, {
            "$set": {
                "address_district": "",
                "address_subdistrict": "",
            }
        })
        totalData+=1


end = datetime.datetime.now()
bench = end - start
print(str(getData) + " data fetched")
print(str(totalData) + " updated in " + str(bench.seconds) + " seconds")