import pymongo
import xlrd
import datetime
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolRegis = mydb["ppdb_registrations_2020"]
mycolJunior = mydb["ppdb_unjuniors"]


# regisArray = mycolRegis.find({"level_pendaftaran": "nhun", "tahap_pendaftaran": 2,
#     "status": {"$in": ["new", "approved"]}})
regisArray = mycolRegis.find({"code":"400263-2-557"})

for regis in regisArray:
    if "rapor" in regis:
        if len(regis["rapor"]) > 0:
            totalRapor = 0
            for rapor in regis["rapor"]:
                if "score_bahasa" in rapor:
                    totalRapor += float(rapor["score_bahasa"])
                if "score_english" in rapor:
                    totalRapor += float(rapor["score_english"])
                if "score_math" in rapor:
                    totalRapor += float(rapor["score_math"])
                if "score_physics" in rapor:
                    totalRapor += float(rapor["score_physics"])
                if "score_religion" in rapor:
                    totalRapor += float(rapor["score_religion"])
                if "score_pancasila" in rapor:
                    totalRapor += float(rapor["score_pancasila"])
                if "score_science" in rapor:
                    totalRapor += float(rapor["score_science"])
            if totalRapor > 0:
                mycolRegis.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": {
                        "total_rapor": totalRapor
                    }
                })
                print(regis["_id"])