import pymongo
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolOptions = mydb["ppdb_options"]
mycolRegistrant = mydb["ppdb_registrations_2020"]
mycolCounters = mydb["counters"]


regisArray = mycolRegistrant.find({"code": {"$exists": False}})
switcher = {
        "abk": 5,
        "ketm": 7,
        "perpindahan": 3,
        "anak-guru": 9,
        "nhun": 2,
        "prestasi": 1,
        "zonasi-jarak": 4,
        "slb": 8,
        "tenaga-kesehatan":11,
        "nhun-unggulan": 10
    }


for regis in regisArray:
    if "first_choice" in regis:
        optionAggregate = mycolOptions.aggregate([
            {
                "$match": {
                    "_id": regis["first_choice"]
                }
            },
            {
                "$lookup": {
                    "from": "ppdb_schools",
                    "localField": "school_id",
                    "foreignField": "_id",
                    "as": "first_school"
                }
            },  
            {
                "$unwind": "$first_school"
            },
        ])

        for option in optionAggregate:
           code_school=option["first_school"]["code"]
           school_level=option["first_school"]["level"]
           first_choice_school=option["first_school"]["_id"]
           obj={
               "code_school":str(code_school),
               "school_level":school_level,
               "first_choice_school":first_choice_school,
               "code_type": switcher.get(str(option["type"]).lower())
           }
           
           if regis["jenjang_pendaftaran"] == "vocational" : obj.update({"major_vocational": option["name"]})
            
           count = mycolCounters.find_one({
               "id": "code_registrant_seq",
               "reference_value.code_school": str(obj["code_school"]),
               "reference_value.code_type": obj["code_type"]
           })

           if count is not None:
               seq = count["seq"] + 1
               obj.update({"code_registrant": seq})
               obj.update({"code": obj["code_school"] + "-" + str(obj["code_type"]) + "-" + str(seq)})
               mycolRegistrant.update_one({
                    "_id": regis["_id"]
               }, {
                    "$set": obj
               })

               mycolCounters.update_one({
                   "_id": count["_id"]
               }, {
                   "$set": {
                       "seq": seq
                   }
               })
               print(regis["_id"])

            

            
# arraySchool = mycolSchools.find({"level": "vocational"})

# for school in arraySchool:
#     arrayRegistrant = mycolRegistrant.find({
#         "first_choice_school": school["_id"],
#         "level_pendaftaran": "nhun-unggulan"
#     })

#     i=1
#     codeschool = str(school["code"])
#     for regis in arrayRegistrant:
#         mycolRegistrant.update_one({
#             "_id": regis["_id"]
#         }, {
#             "$set": {
#                 "code_type": 10,
#                 "code_registrant": i,
#                 "code": codeschool+"-"+"10"+"-"+str(i)
#             }
#         })
#         i+=1

#     if codeschool != "":
#         mycolCounters.update_one({
#             "id": "code_registrant_seq",
#             "reference_value.code_school": codeschool,
#             "reference_value.code_type": 10
#         }, {
#             "$set": {
#                 "seq": i
#             }
#         },upsert=True)
#     codeschool=""
# print(school["_id"])