import pymongo
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolJunior = mydb["ppdb_unjuniors"]
mycolRegistration = mydb["ppdb_registrations_2020"]

registrant = mycolRegistration.find({"level_pendaftaran": {"$in": ["nhun", "abk", "zonasi-jarak"]}, "tahap_pendaftaran": 2,
    "status": {"$in": ["new", "approved"]}})

i=0
for regis in registrant:
    if "id_junior" in regis:
        junior = mycolJunior.find_one({"_id": regis["id_junior"]})
        if junior is not None:
            if "kk_file" in junior:
                mycolRegistration.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": {
                        "no_kk": junior["no_kk"],
                        "kk_file": junior["kk_file"],
                    }
                })
            if "tanggal_kk" in junior:
                if junior["tanggal_kk"] is not "Invalid date" :
                    mycolRegistration.update_one({
                        "_id": regis["_id"]
                    }, {
                        "$set": {
                            "tanggal_kk": junior["tanggal_kk"],
                        }
                    })
            if "status_keluarga" in junior:
                mycolRegistration.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": {
                        "status_keluarga": junior["status_keluarga"],
                    }
                })
            i+=1
            print(i)
# i=0
# for regis in registrant:
    # if "id_junior" in regis:
    #     junior = mycolJunior.find_one({
    #         "_id": regis["id_junior"]
    #     })

    #     if "rapor" in junior:
    #         if len(junior["rapor"]) > 0:
    #             mycolRegistration.update_one({
    #                 "_id": regis["_id"]
    #             }, {
    #                 "$set" : {
    #                     "rapor": junior["rapor"]
    #                 }
    #             })
    #     i+=1
    #     print(i)