import pymongo
import xlrd
import datetime
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolschools = mydb["ppdb_schools"]
mycoljunior = mydb["ppdb_unjuniors"]

slbSchool = mycolschools.find({"level": "slb"})
code = 500001
for slb in slbSchool:
    mycolschools.update_one({"_id": slb["_id"]}, {
        "$set": {
            "code": code
        }
    })
    code+=1


juniorSlb = mycoljunior.find({
    "un_type": "slb"
})

for slb in juniorSlb:
    try:
        print(slb['name'])
        jenjang=slb['slb_jenjang']
        un=slb['un'].split("-")
        unsplit = un[0]+"-"+un[1]+"-"+un[2]
        regexsplit =  "^"+unsplit

        
        if jenjang=="SD-LB":
            sc = mycolschools.find_one( {"school_un_sd": {"$regex":regexsplit} })
            if sc is not None:
                mycoljunior.update_one({
                    "_id": slb["_id"]
                }, {
                    "$set": {
                        "school_id": sc["_id"]
                    }
                })
        else:
            sc = mycolschools.find_one({"school_un_smp": {"$regex":regexsplit} })
            if sc is not None:
                mycoljunior.update_one({
                    "_id": slb["_id"]
                }, {
                    "$set": {
                        "school_id": sc["_id"]
                    }
                })
    except:
        pass
print("Done")

# for fileName in files:
#     dirname = os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__)), dir), fileName)
#     print(dirname)