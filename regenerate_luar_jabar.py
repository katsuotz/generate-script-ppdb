import pymongo
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolJunior = mydb["ppdb_unjuniors"]

registrant = mycolJunior.find({"un": {"$regex": "^30-"}, "luar_jabar": {"$exists": False}})

i=0
for regis in registrant:
    i+=1
    mycolJunior.update_one({
        "_id": regis["_id"]
    }, {
        "$set": {
            "luar_jabar": 1
        }
    })
    print(i)