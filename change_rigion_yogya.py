import pymongo
import xlrd
import datetime
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolRegion = mydb["ppdb_regions"]

regionList = mycolRegion.find({"province": "DAERAH ISTIMEWA"})

for region in regionList:
    mycolRegion.update_one({
        "_id": region["_id"]
    }, {
        "$set": {
            "province": "DAERAH ISTIMEWA YOGYAKARTA"
        }
    })
    print(region["_id"])