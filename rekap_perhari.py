import pymongo
import json
import os
import datetime
from dateutil import relativedelta

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolJunior = mydb["ppdb_unjuniors"]
mycolRegistration = mydb["ppdb_registrations_2020"]

registrant = mycolRegistration.find({})

# def calculateAge(birthDate): 
#     days_in_year = 365.2425    
#     age = int((datetime.date.today() - birthDate).days / days_in_year) 
#     return age 
          
i=0
for regis in registrant:
    if "id_junior" in regis:
        junior = mycolJunior.find_one({
            "_id": regis["id_junior"]
        })

        if "document" in junior:
            mycolRegistration.update_one({
                "_id": regis["_id"]
            }, {
                "$set" : {
                    "document": junior["document"]
                }
            })
        i+=1
        print(i)