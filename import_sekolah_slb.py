from dbfread import DBF
import pymongo
import json
import os

dataSchool=[]

def deleteFile(fileName):
    if os.path.isfile(fileName):
        os.remove(fileName)
        print("File Removed!")
    else:
        print ("File not exist")

def getaddresscity(kd_rayon):
    switcher={
        1: 'KOTA BANDUNG',
        2: "KOTA BANJAR",
        3: "KOTA BEKASI",
        4: "KOTA BOGOR",
        5: "KOTA CIMAHI",
        6: "KOTA CIREBON",
        7: "KOTA DEPOK",
        8: "KOTA SUKABUMI",
        9: "KOTA TASIKMALAYA",
        10: "KAB. BANDUNG",
        11: "KAB. BANDUNG BARAT",
        12: "KAB. BEKASI",
        13: "KAB. BOGOR",
        14: "KAB. CIAMIS",
        15: "KAB. CIANJUR",
        16: "KAB. CIREBON",
        17: "KAB. GARUT",
        18: "KAB. INDRAMAYU",
        19: "KAB. KARAWANG",
        20: "KAB. KUNINGAN",
        21: "KAB. MAJALENGKA",
        22: "KAB. PURWAKARTA",
        23: "KAB. SUBANG",
        24: "KAB. SUKABUMI",
        25: "KAB. SUMEDANG",
        26: "KAB. TASIKMALAYA",
        27: "KAB. PANGANDARAN",
    }
    return switcher.get(kd_rayon, "-")

def createJSONschoolSD():
    dir = './SLB/SD LB JBR/SEK_021152020114324'
    allfiles = os.listdir(dir)
    files = [ fname for fname in allfiles if fname.endswith('.DBF')]
    npsnSchool = []

    negriSD = 0
    for fileName in files:
        dirname = os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__)), dir), fileName)
        print(dirname)
        for record in DBF(dirname, load=True):
            # codeSchool += 1
            
            if record['STS_SEK'] == "N":
                negriSD+=1
            dataSchool.append({
                "type": "open" if record['STS_SEK'] == "N" else "private",
                "level": "slb",
                # "code": codeSchool,
                "school_un_sd": record['KD_PROP'] + "-" +record['KD_RAYON'] + "-" + record['KD_SEK'],
                "name": record['NM_SEK'],
                "address": record['ALAMAT_1'],
                "address_city": getaddresscity(int(record['KD_RAYON'])),
                "address_province": "JAWA BARAT",
                "address_district": "",
                "address_subdistrict": "",
                "address_rw": "00",
                "address_rt": "00",
                "coordinate_lat": "0.0",
                "coordinate_lng": "0.0",
                "permission": "000",
                "npsn": record['NPSN'],
            })
            # print(codeSchool)

    with open('data_school_slb.json', 'w') as outfile:
        json.dump(dataSchool, outfile)

    print("negriSD " + str(negriSD))
    print("Done")
    # return codeSchool
 
def createJSONschoolSMP():
    dir = './SLB/SMP LB JBR/SEK_021152020114337'
    allfiles = os.listdir(dir)
    files = [ fname for fname in allfiles if fname.endswith('.DBF')]
    
    # codeSchool = code
    npsnSchool = []
    negriSMPLB = 0
    for fileName in files:
        dirname = os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__)), dir), fileName)
        print(dirname)
        for record in DBF(dirname, load=True):
            # codeSchool += 1
            if record['STS_SEK'] == "N":
                negriSMPLB+=1
            dd = {
                "type": "open" if record['STS_SEK'] == "N" else "private",
                "level": "slb",
                # "code": codeSchool,
                "school_un_smp": record['KD_PROP'] + "-" +record['KD_RAYON'] + "-" + record['KD_SEK'],
                "name": record['NM_SEK'],
                "address": record['ALAMAT_1'],
                "address_city": getaddresscity(int(record['KD_RAYON'])),
                "address_province": "JAWA BARAT",
                "address_district": "",
                "address_subdistrict": "",
                "address_rw": "00",
                "address_rt": "00",
                "coordinate_lat": "0.0",
                "coordinate_lng": "0.0",
                "permission": "000",
                "school_un": "123",
                "npsn": record['NPSN'],
            }
            # if dd not in dataSchool:
            dataSchool.append(dd)
            # print(codeSchool)
    print("Negeri SMP " + str(negriSMPLB))
def main():
    createJSONschoolSD()
    createJSONschoolSMP()
    

    for slb in dataSchool:
        if findOne(slb, newArraySMP) == False:
            newArraySMP.append(slb)

    for s in newArraySMP:
        sc = myschool.find_one({"level": "slb","name": s['name'],"address_city": s['address_city']})
        if sc is None:
            myschool.insert_one(s)
        else:
            myschool.update_one({"_id": sc['_id']}, {
                "$set": {
                    "school_un_smp": s['school_un_smp']
                }
            })
    print("Done")

def findOne(obj, arr):
    for a in arr:
        if a['name'] == obj['name'] and a['address_city'] == obj['address_city'] : 
            return True
    return False

if __name__ == '__main__':
    main()