from dbfread import DBF
import json
import os
import pymongo
import datetime
from bson import json_util

school = []

for record in DBF("./PAKET A/SEK20_0205A.DBF"):
    code = 10126
    school.append({
                "type": "open" if record['STS_SEK'] == "N" else "private",
                "level": "elementary",
                "code": code,
                "school_un_smp": record['KD_PROP'] + "-" +record['KD_RAYON'] + "-" + record['KD_SEK'],
                "name": record['NM_SEK'],
                "address": record['ALAMAT_1'],
                "address_city": "KOTA CIMAHI",
                "address_province": "JAWA BARAT",
                "address_district": "",
                "address_subdistrict": "",
                "address_rw": "00",
                "address_rt": "00",
                "coordinate_lat": "0.0",
                "coordinate_lng": "0.0",
                "permission": "000",
                "school_un": "",
                "npsn": record['NPSN'],
                "paket_a": True,
            })
    code += 1

myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
mydb = myclient["ppdbCimahi"]
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mySchool = mydb["ppdb_schools"]
myUser = mydb["ppdb_users"]

for sc in school: 
    schoolPaketA = mySchool.find_one({
        "name": sc["name"]
    })
    if schoolPaketA is not None:
        mySchool.update_one({
            "_id": schoolPaketA["_id"]
        }, {
            "$set": sc
        })
        print("Hallo If ")
    else:
        sccccc =  mySchool.insert_one(sc)
        myUser.insert_one({
            "name": sc["name"],
            "username": "CMHSD1"+sc["npsn"],
            "password": "CMHSD1"+sc["npsn"],
            "role": "elementaryschool-admin",
            "school": str(sccccc.inserted_id)
        })
        print("Hallo Else "+str(sccccc.inserted_id))