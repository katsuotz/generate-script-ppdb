from dbfread import DBF
import json
import pymongo
import os
import datetime
from bson import json_util

def main():
    print("Main")
    #myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    #mydb = myclient["ppdb_cimahi"]
    myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
    mydb = myclient["ppdbCimahi"]
    #myclient = pymongo.MongoClient("mongodb://ppdbCimahiDev:1@localhost:27017/ppdbCimahiDev")
    #mydb = myclient["ppdbCimahiDev"]
    myOptions = mydb["ppdb_options"]
    options = myOptions.find({"type": "afirmasi"})
    i = 0
    for opt in options:
        name = str(opt["name"]).replace("KETM", "AFIRMASI")
        myOptions.update_one({
            "_id": opt["_id"]
        }, {
            "$set": {
                "name": name,
            }
        })
        i+=1
        print(i)



if __name__ == '__main__':
    main()
