from dbfread import DBF
import pymongo
import xlrd
import json
import os

dataSchool=[]
#myclient = pymongo.MongoClient("mongodb://ppdbCimahiDev:1@localhost:27017/ppdbCimahiDev")
#mydb = myclient["ppdbCimahiDev"]
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb_cimahi"]
myUser = mydb["ppdb_users"]
mySchools = mydb["ppdb_schools"]

dirname = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sekolah.xlsx")
print(dirname)
wb = xlrd.open_workbook(dirname) 
sh = wb.sheet_by_index(0)


index=0
for i in range(sh.nrows):
    if i >= 1:
        value = sh.row_values(i)
        rapor=[]
        dataSchool.append({
            "type": "open" if str(value[9]).replace(".0", "") == "1" else "private",
            "level": "sd",
            # "code": codeSchool,
            "school_un": str(value[0]) + "-" + str(value[1]) + "-" + str(value[3]),
            "name":value[4],
            "address": value[12],
            "address_city": "",
            "address_province": "JAWA BARAT",
            "address_district": "",
            "address_subdistrict": "",
            "address_rw": "00",
            "address_rt": "00",
            "coordinate_lat": "0.0",
            "coordinate_lng": "0.0",
            "permission": "000",
            "npsn": str(value[9]).replace(".0", ""),
        })

mySchools.insert_many(dataSchool)

schools = mySchools.find({"level": "sd"})

for sc in schools:
    myUser.insert_one({
        "name": sc["name"],
        "username": "CMHSD"+sc["npsn"],
        "password": "CMHSD"+sc["npsn"],
        "role": "elementaryschool-admin",
        "school": str(sc["_id"])
    })