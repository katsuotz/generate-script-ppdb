from dbfread import DBF
import pymongo
import xlrd
import json
import os

dataRapor=[]
myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
mydb = myclient["ppdbCimahi"]
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
#mydb = myclient["ppdb_cimahi"]
myJunior = mydb["ppdb_unjuniors"]

def checkValidationScore(score):
    score = str(score).strip()
    if score == '':
        return 0
    else:
        return "{:.2f}".format(float(score))

dataRapor = []

unjuniors = myJunior.find({})

for junior in unjuniors:
    rapor = []
    if "rapor" in junior:
        for r in junior["rapor"]:
            rapor.append({
                "sms": int(float(r["sms"])),
                "nilai": float(checkValidationScore(r["nilai"]))
            })
        print(junior["_id"])
        myJunior.update_one({"_id": junior["_id"]}, { "$set": { "rapor": rapor } })


#mySchools.insert_many(dataRapor)

#schools = mySchools.find({"level": "tk"})

#for sc in schools:
#    myUser.insert_one({
#        "name": sc["name"],
#        "username": "TK"+sc["npsn"],
#        "password": "TK"+sc["npsn"],
#        "role": "tkschool-admin",
#        "school": str(sc["_id"])
#    })