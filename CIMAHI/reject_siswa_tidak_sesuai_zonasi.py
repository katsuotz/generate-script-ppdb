from dbfread import DBF
import json
import pymongo
import os
import datetime
from bson import json_util
from datetime import datetime

def main():
    print("Main")
    myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
    mydb = myclient["ppdbCimahi"]
    #myclient = pymongo.MongoClient("mongodb://ppdbCimahiDev:1@localhost:27017/ppdbCimahiDev")
    #mydb = myclient["ppdbCimahiDev"]
    #myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    #mydb = myclient["ppdb_cimahi"]
    myRegis = mydb['ppdb_registrations_2020']

    dataRegis = myRegis.aggregate([
    {
        "$match": {
            "$or": [
            
                {"level_pendaftaran": "zonasi"},
                {"level_pendaftaran": "abk"}
            ],
						"score_jarak_1" :{
							"$gt" : 500
						},
        }
    },
    {
        "$lookup": 
        {
            "from": "ppdb_schools",
            "localField": "first_choice_school",
            "foreignField": "_id",
            "as": "sekolah"
        }
    },
    {
        "$unwind": {
            "path": "$sekolah"
        }
    },
		
    {
        "$project": {
						"status" : "$status",
						"waktu" : "$createdAt",
						"seleksi" : "$status_seleksi",
						"kecPendaftar" : "$address_district",
						"kecSekolah" : "$sekolah.address_district",
						"scoreJarak" : "$score_jarak_1",
						"kecamatanNama" : "$sekolah.name",
            "kecamatanSama": {
							"$cmp": ['$address_district', '$sekolah.address_district']
						},
        }
    },
    {"$match": {"kecamatanSama": {"$ne": 0}}} 
    ])

    for regis in dataRegis:
        myRegis.update_one({
            "_id": regis["_id"]
        }, {
            "$set" :{
                "status": "reject",
                "alasan": "Pendaftar diluar zonasi dengan sekolah tujuan dan lebih dari 500 meter"
            }
        })
        print(regis["_id"])


if __name__ == '__main__':
    main()
