from dbfread import DBF
import json
import pymongo
import os
import datetime
from bson import json_util

def main():
    print("Main")
    myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
    mydb = myclient["ppdbCimahi"]
    myRegis = mydb["ppdb_registrations_2020"]
    blankRegis = myRegis.find({"status": {"$exists": False}})
    for regis in blankRegis:
        myRegis.update_one({
            "_id": regis["_id"]
        }, {
            "$set": {
                "status": "new"
            }
        })
        print(regis["_id"])


if __name__ == '__main__':
    main()
