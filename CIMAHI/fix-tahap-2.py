from dbfread import DBF
import json
import pymongo
import os
import datetime
from bson import json_util
from datetime import datetime

def checkValidationScore(score):
    score = str(score).strip()
    if score == '':
        return 0
    else:
        return float("{:.2f}".format(float(score)))


def main():
    print("Main")
    myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
    mydb = myclient["ppdbCimahi"]
    #myclient = pymongo.MongoClient("mongodb://ppdbCimahiDev:1@localhost:27017/ppdbCimahiDev")
    #mydb = myclient["ppdbCimahiDev"]
    #myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    #mydb = myclient["ppdb_cimahi"]
    myOptions = mydb['ppdb_options']
    myRegis = mydb['ppdb_registrations_2020']

    level = ["prestasi-akademis", "prestasi-non-akademis", "ketm"]

    myRegis.update_many({
        "level_pendaftaran": {"$nin": level}
    }, {
        "$set": {
            "tahap_pendaftaran": 2
        }
    })

    myRegis.update_many({
        "level_pendaftaran": {"$in": level}
    }, {
        "$set": {
            "tahap_pendaftaran": 1
        }
    })



if __name__ == '__main__':
    main()
