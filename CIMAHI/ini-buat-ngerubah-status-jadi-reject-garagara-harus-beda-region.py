import json
import pymongo
import os
import datetime
from bson import json_util

def main():
    print("Main")
    #myclient = pymongo.MongoClient("mongodb://ppdbCimahiDev:1@localhost:27017/ppdbCimahiDev")
    #mydb = myclient["ppdbCimahiDev"]
    myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
    mydb = myclient["ppdbCimahi"]
    my_registration = mydb["ppdb_registrations_2020"]

    dataRegis = my_registration.aggregate([
    {
        "$match": {
           "$or": [
            { "level_pendaftaran": "prestasi-non-akademis"},
            { "level_pendaftaran": "prestasi-akademis"},
            ]
        }
    },
    {
        "$lookup": 
        {
            "from": "ppdb_schools",
            "localField": "first_choice_school",
            "foreignField": "_id",
            "as": "sekolah"
        }
    },
    {
        "$unwind": {
            "path": "$sekolah"
        }
    },
		
    {
        "$project": {
					"status" : "$status",
					"waktu" : "$createdAt",
					"seleksi" : "$status_seleksi",
					"kecPendaftar" : "$address_district",
					"kecSekolah" : "$sekolah.address_district",
                    "kecamatanSama": {
						"$cmp": ['$address_district', '$sekolah.address_district']
					},
        }
    },
    {"$match": {"kecamatanSama": {"$eq": 0}}} 
    ])

    for regis in dataRegis:
        my_registration.update_one({
            "_id": regis["_id"]
        }, {
            "$set": {
                "status_seleksi": 0,
                "status": "reject",
                "alasan": "Zona sama, harap didaftarkan dijalur zonasi"
            }
        })
        print(regis["_id"])

if __name__ == '__main__':
    main()
