from dbfread import DBF
import json
import pymongo
import os
import datetime
from bson import json_util

def main():
    print("Main")

    myclient = pymongo.MongoClient("mongodb://ppdbCimahi:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdbCimahi")
    mydb = myclient["ppdbCimahi"]
    #myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    #mydb = myclient["ppdb_cimahi"]
    #myclient = pymongo.MongoClient("mongodb://ppdbCimahiDev:1@localhost:27017/ppdbCimahiDev")
    #mydb = myclient["ppdbCimahiDev"]
    myOptions = mydb["ppdb_options"]
    myschool = mydb["ppdb_schools"]
    #sdOptios = ["ZONASI","ABK","ZONASI PERBATASAN", "AFIRMASI", "PERPINDAHAN & ANAK GURU"]
    #sdtype = ["zonasi","abk","zonasi-perbatasan", "ketm", "perpindahan"]

    #smpOptios = ["ZONASI","ABK", "ZONASI PERBATASAN", "AFIRMASI", "PRESTASI AKADEMIS", "PRESTASI NON AKADEMIS", "PERPINDAHAN & ANAK GURU"]
    #smptype = ["zonasi","abk","zonasi-perbatasan", "ketm", "prestasi-akademis", "prestasi-non-akademis", "perpindahan"]

    sdOptios = ["TENAGA KESEHATAN"]
    sdtype = ["tenaga-kesehatan"]

    smpOptios = ["TENAGA KESEHATAN"]
    smptype = ["tenaga-kesehatan"]

    scArray = myschool.find({
        "level": {
            "$ne": "tk"
        }
    })
    for school in scArray:
        if "komplek" not in school:
            opt = myOptions.find_one({"school_id": school["_id"], "type": "tenaga-kesehatan"})
            if opt is None:
                if (school["level"] == "elementary"):
                    i = 0
                    for option in sdOptios:
                        name = school["name"] + " - " + option
                        obj = {
                            "verified_cadisdik" : True,
                            "no_color_blind" : 0,
                            "name" : name,
                            "type" : sdtype[i],
                            "rombel" : 0,
                            "quota" : 0,
                            "total_quota" : 0,
                            "method" : sdtype[i],
                            "school_id" : school["_id"],
                            "major_id" : 0,
                            "quota_per_class" : 0,
                            "quota_new" : 0,
                            "total_quota_new" : 0,
                            "current_student_new" : 0,
                            "quota_per_class_new" : 0,
                            "__v" : 0
                        }
                        myOptions.insert_one(obj)
                        i+=1
                elif school["level"] == "junior":
                    i = 0
                    for option in smpOptios:
                        name = school["name"] + " - " + option
                        obj = {
                            "verified_cadisdik" : True,
                            "no_color_blind" : 0,
                            "name" : name,
                            "type" : smptype[i],
                            "rombel" : 0,
                            "quota" : 0,
                            "total_quota" : 0,
                            "method" : smptype[i],
                            "school_id" : school["_id"],
                            "major_id" : 0,
                            "quota_per_class" : 0,
                            "quota_new" : 0,
                            "total_quota_new" : 0,
                            "current_student_new" : 0,
                            "quota_per_class_new" : 0,
                            "__v" : 0
                        }
                        myOptions.insert_one(obj)
                        i+=1



if __name__ == '__main__':
    main()
