import pymongo
import json
import os

myclient = pymongo.MongoClient("mongodb://ppdb:f4b341634c0ee5a6b688f701779a7a49e4b09a0e@localhost:27017/ppdb")
#myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["ppdb"]
mycolJunior = mydb["ppdb_unjuniors"]
mycolRegistration = mydb["ppdb_registrations_2020"]

registrant = mycolRegistration.find({"luar_jabar" : 1})

i=0
for regis in registrant:
    if regis["id_junior"] is not None:
        junior = mycolJunior.find_one({"_id": regis["id_junior"]})
        if junior is not None:
            
            if "lulus" in junior:
                print(regis["id_junior"])   
                mycolRegistration.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": {
                        "lulus": junior["lulus"]
                    }
                })
            if "skl_file" in junior:
                mycolRegistration.update_one({
                    "_id": regis["_id"]
                }, {
                    "$set": {
                        "skl_file": junior["skl_file"],
                        "lulus": 1
                    }
                })